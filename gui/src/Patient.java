import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.*;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import uk.ac.manchester.cs.owl.owlapi.OWLImportsDeclarationImpl;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;

public class Patient {

	public static class PatientAlreadyExistsException extends Exception {
		public String getMessage() {
			return "patient already exists";
		}
	}

	public static class PatientNotFoundException extends Exception {
		public String getMessage() {
			return "patient not found";
		}
	}

	private final String               name;
	private final File                 file;
	private       IRI                  iri;
	private       OWLOntology          ontology;
	private       OWLOntologyManager   manager;
	private       OWLDataFactory       df;
	private       DefaultPrefixManager pm;

	private       OWLNamedIndividual   thePatient;
	private       int                  individualMaxNumber;
	private       OWLNamedIndividual   currentSession;

	private       OWLReasoner          reasoner;

	public Patient(String name, File file, boolean create) throws OWLOntologyCreationException, PatientAlreadyExistsException, PatientNotFoundException {
		if (file.exists() && create) {
			throw new PatientAlreadyExistsException();
		}
		if (!file.exists() && !create) {
			throw new PatientNotFoundException();
		}
		this.name = name;
		this.file = file;

		manager = new OWLManager().createOWLOntologyManager();

		manager.addIRIMapper(new SimpleIRIMapper(Misc.IRI_SCHIZO, IRI.create(Misc.FILE_SCHIZO)));
		manager.addIRIMapper(new SimpleIRIMapper(Misc.IRI_TIME,   IRI.create(Misc.FILE_TIME)));

		df = manager.getOWLDataFactory();

		iri = IRI.create(Misc.STR_PREFIX + name);

		pm = new DefaultPrefixManager(iri.toString());
		pm.setPrefix("schizo:", Misc.IRI_SCHIZO.toString() + "#");
		pm.setPrefix("time:",   Misc.IRI_TIME  .toString() + "#");

		if (create) {
			createPatient();
		} else {
			ontology = manager.loadOntologyFromOntologyDocument(file);
		}

		// get maximum counter value
		Set<OWLNamedIndividual> individuals = ontology.getIndividualsInSignature();
		individualMaxNumber = 0;
		for (OWLNamedIndividual indiv : individuals) {
			String[] indivNameParts = Misc.entityName(indiv).split("_");
			try {
				individualMaxNumber = Math.max(individualMaxNumber, Integer.parseInt(indivNameParts[indivNameParts.length - 1]));
			} catch (Exception e) {
				// do nothing
			}
		}

		// find patient individual
		OWLNamedIndividual foundPatient = null;
		for (OWLNamedIndividual i : individuals) {
			if (i.getTypes(ontology).contains(df.getOWLClass("schizo:Patient", pm))) {
				foundPatient = i;
				break;
			}
		}
		thePatient = foundPatient;
		if (thePatient == null) {
			throw new OWLOntologyCreationException();
		}

		// find current session
		OWLNamedIndividual foundSession = null;
		for (OWLNamedIndividual i : individuals) {
			if (i.getTypes(ontology).contains(df.getOWLClass("time:Session", pm))
			 && i.getTypes(ontology).contains(df.getOWLClass("time:CurrentEvent", pm))) {
				foundSession = i;
				break;
			}
		}
		currentSession = foundSession;
		if (currentSession == null) {
			throw new OWLOntologyCreationException();
		}

		// update durations
		for (OWLNamedIndividual ind : individuals) {
			if (ind.getTypes(ontology).contains(df.getOWLClass("time:CurrentEvent", pm))) {
				Set<OWLLiteral> maybeDuration = ind.getDataPropertyValues(df.getOWLDataProperty("time:hasDuration", pm), ontology);
				Set<OWLLiteral> maybeBeginning = ind.getDataPropertyValues(df.getOWLDataProperty("time:hasBeginning", pm), ontology);
				for (OWLLiteral duration : maybeDuration) {
					manager.removeAxiom(ontology, df.getOWLDataPropertyAssertionAxiom(df.getOWLDataProperty("time:hasDuration", pm), ind, duration));
				}
				for (OWLLiteral beginning : maybeBeginning) {
					// should be only one or none
					int duration = Misc.calendarToInt(Misc.today()) - beginning.parseInteger();
					manager.addAxiom(ontology, df.getOWLDataPropertyAssertionAxiom(df.getOWLDataProperty("time:hasDuration", pm), ind, duration));
				}
			}
		}

		reasoner = new PelletReasoner(ontology, BufferingMode.NON_BUFFERING);

		System.out.println("reasoner: " + reasoner.getReasonerName());

		if (create) {
			System.out.println("created patient " + name);
		} else {
			System.out.println("loaded patient " + name);
		}
	}

	private void createPatient() throws OWLOntologyCreationException {
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

		OWLNamedIndividual  individual = df.getOWLNamedIndividual("#" + name, pm);
		OWLDeclarationAxiom declAxiom  = df.getOWLDeclarationAxiom(individual);
		OWLAxiom            classAxiom = df.getOWLClassAssertionAxiom(df.getOWLClass("schizo:Patient", pm), individual);

		currentSession = df.getOWLNamedIndividual("#Session_1", pm);
		OWLDeclarationAxiom s_declAxiom  = df.getOWLDeclarationAxiom(currentSession);
		OWLAxiom            s_classAxiom = df.getOWLClassAssertionAxiom(df.getOWLClass("time:Session", pm), currentSession);
		OWLAxiom            s_currAxiom  = df.getOWLClassAssertionAxiom(df.getOWLClass("time:CurrentEvent", pm), currentSession);

		axioms.add(declAxiom);
		axioms.add(classAxiom);
		axioms.add(s_declAxiom);
		axioms.add(s_classAxiom);
		axioms.add(s_currAxiom);

		ontology = manager.createOntology(axioms, iri);

		OWLImportsDeclaration importSchizo = df.getOWLImportsDeclaration(Misc.IRI_SCHIZO);
		OWLImportsDeclaration importTime   = df.getOWLImportsDeclaration(Misc.IRI_TIME);

		manager.applyChange(new AddImport(ontology, importSchizo));
		manager.applyChange(new AddImport(ontology, importTime));

		keepAllIndividualsDifferent();
	}

	private void reload() {
		save();
		manager = new OWLManager().createOWLOntologyManager();

		manager.addIRIMapper(new SimpleIRIMapper(Misc.IRI_SCHIZO, IRI.create(Misc.FILE_SCHIZO)));
		manager.addIRIMapper(new SimpleIRIMapper(Misc.IRI_TIME,   IRI.create(Misc.FILE_TIME)));

		df = manager.getOWLDataFactory();

		iri = IRI.create(Misc.STR_PREFIX + name);

		pm = new DefaultPrefixManager(iri.toString());
		pm.setPrefix("schizo:", Misc.IRI_SCHIZO.toString() + "#");
		pm.setPrefix("time:",   Misc.IRI_TIME  .toString() + "#");

		try {
			ontology = manager.loadOntologyFromOntologyDocument(file);
		} catch (OWLOntologyCreationException e) {
			System.err.println("SOMETHING WENT TERRIBLY WRONG");
			e.printStackTrace();
		}

		reasoner = new PelletReasoner(ontology, BufferingMode.NON_BUFFERING);

		System.out.println("reasoner: " + reasoner.getReasonerName());

		System.out.println("reloaded patient " + name);
	}

	public OWLOntology getOntology() {
		return ontology;
	}

	public OWLOntologyManager getOntologyManager() {
		return manager;
	}
	
	public File getFile() {
		return file;
	}

	public String getName() {
		return name;
	}

	public IRI getIRI() {
		return iri;
	}

	public String getIRIString() {
		return iri.toString();
	}

	public boolean save() {
		try {
			manager.saveOntology(ontology, new RDFXMLOntologyFormat(), new FileOutputStream(file));
			System.out.println("saved patient " + name + " to " + file);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (OWLOntologyStorageException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean endSession() {
		// close old session
		OWLAxiom currentAxiom   = df.getOWLClassAssertionAxiom(df.getOWLClass("time:CurrentEvent",   pm), currentSession);
		OWLAxiom completedAxiom = df.getOWLClassAssertionAxiom(df.getOWLClass("time:CompletedEvent", pm), currentSession);
		if (manager.removeAxiom(ontology, currentAxiom).isEmpty()) {
			return false;
		}
		manager.addAxiom   (ontology, completedAxiom);

		// create new session
		individualMaxNumber++;

		currentSession = df.getOWLNamedIndividual("#Session_" + individualMaxNumber, pm);
		OWLDeclarationAxiom s_declAxiom  = df.getOWLDeclarationAxiom(currentSession);
		OWLAxiom            s_classAxiom = df.getOWLClassAssertionAxiom(df.getOWLClass("time:Session", pm), currentSession);
		OWLAxiom            s_currAxiom  = df.getOWLClassAssertionAxiom(df.getOWLClass("time:CurrentEvent", pm), currentSession);

		manager.addAxiom(ontology, s_declAxiom);
		manager.addAxiom(ontology, s_classAxiom);
		manager.addAxiom(ontology, s_currAxiom);

		keepAllIndividualsDifferent();
		return true;
	}
	
	public boolean excludeConstraint(Constraint c) {
		String role = c.getObjectSuperType().replace("#", "#has");

		Set<OWLClassExpression> types = new HashSet<OWLClassExpression>();
		types.add((OWLClassExpression) df.getOWLClass(IRI.create(c.getObjectType())));
		for (Map.Entry<String,Boolean> entry : c.getBoolMods().entrySet()) {
			if (entry.getValue()) {
				types.add(df.getOWLClass(IRI.create(entry.getKey())));
			} else {
				types.add(df.getOWLObjectComplementOf(df.getOWLClass(IRI.create(entry.getKey()))));
			}
		}

		OWLClassExpression type = df.getOWLObjectIntersectionOf(types);
		
		OWLClassAssertionAxiom ax = df.getOWLClassAssertionAxiom(
				df.getOWLObjectComplementOf(df.getOWLObjectSomeValuesFrom(df.getOWLObjectProperty(IRI.create(role)), type)),
				thePatient);
		manager.addAxiom(ontology, ax);
		reload();
		if (!reasoner.isConsistent()) {
			manager.removeAxiom(ontology, ax);
			return false;
		}
		return true;
	}

	public boolean removeConstraint(Constraint c) {
		OWLEntityRemover remover = new OWLEntityRemover(manager, Collections.singleton(ontology));
		((OWLNamedIndividual) c.getObject()).accept(remover);
		if (manager.applyChanges(remover.getChanges()).isEmpty()) {
			return false;
		}
		keepAllIndividualsDifferent();
		return true;
	}

	public boolean endConstraint(Constraint c, int endDate) {
		OWLAxiom currentAxiom   = df.getOWLClassAssertionAxiom(df.getOWLClass("time:CurrentEvent",   pm), c.getObject());
		OWLAxiom completedAxiom = df.getOWLClassAssertionAxiom(df.getOWLClass("time:CompletedEvent", pm), c.getObject());
		if (manager.removeAxiom(ontology, currentAxiom).isEmpty()) {
			return false;
		}
		manager.addAxiom(ontology, completedAxiom);
		
		// remove hasDuration axioms
		Set<OWLDataPropertyAssertionAxiom> dpAx = ontology.getDataPropertyAssertionAxioms(c.getObject());
		for (OWLDataPropertyAssertionAxiom ax : dpAx) {
			if (ax.getProperty().equals(df.getOWLDataProperty("time:hasDuration", pm))) {
				manager.removeAxiom(ontology, ax);
			}
		}

		OWLAxiom endAxiom = df.getOWLDataPropertyAssertionAxiom(df.getOWLDataProperty("time:hasEnd", pm), c.getObject(), endDate);
		manager.addAxiom(ontology, endAxiom);
		return true;
	}

	// maybe TODO: adds constraint to current session but not to past ones
	// doesn’t respect Constraint.isCurrentSession
	public boolean addConstraint(Constraint c) throws Exception {
		if (!c.isCurrentSession()) {
			throw new IllegalArgumentException("cannot add constraints to past sessions");
		}

		Set<OWLAxiom> newAxioms = new HashSet<OWLAxiom>();

		OWLClass objectClass = df.getOWLClass(IRI.create(c.getObjectType()));

		individualMaxNumber++;
		OWLNamedIndividual  theObject = df.getOWLNamedIndividual("#" + Misc.baseName(c.getObjectSuperType()) + "_" + Misc.baseName(c.getObjectType()) + "_" + individualMaxNumber, pm);
		OWLDeclarationAxiom declAxiom  = df.getOWLDeclarationAxiom(theObject);
		OWLAxiom            classAxiom = df.getOWLClassAssertionAxiom(objectClass, theObject);

		OWLObjectPropertyAssertionAxiom propAxiom = df.getOWLObjectPropertyAssertionAxiom(
				df.getOWLObjectProperty(IRI.create(c.getObjectSuperType().replace("#", "#has"))), thePatient, theObject);

		OWLObjectPropertyAssertionAxiom sessionAxiom = df.getOWLObjectPropertyAssertionAxiom(
				df.getOWLObjectProperty("time:hasSession", pm), theObject, currentSession);

		newAxioms.add(declAxiom);
		newAxioms.add(classAxiom);
		newAxioms.add(propAxiom);
		newAxioms.add(sessionAxiom);

		for (Map.Entry<String,Boolean> entry : c.getBoolMods().entrySet()) {
			OWLAxiom modAxiom;
			if (entry.getValue()) {
				modAxiom = df.getOWLClassAssertionAxiom(
						df.getOWLClass(IRI.create(entry.getKey())), theObject);
			} else {
				modAxiom = df.getOWLClassAssertionAxiom(
						df.getOWLObjectComplementOf(df.getOWLClass(IRI.create(entry.getKey()))), theObject);
			}
			newAxioms.add(modAxiom);
		}

		for (Map.Entry<String,Integer> entry : c.getIntMods().entrySet()) {
			OWLAxiom modAxiom = df.getOWLDataPropertyAssertionAxiom(
					df.getOWLDataProperty(IRI.create(entry.getKey())), theObject, entry.getValue());
			newAxioms.add(modAxiom);
			if (c.getBoolMods().containsKey(Misc.STR_TIME + "#CurrentEvent") && c.getBoolMods().get(Misc.STR_TIME + "#CurrentEvent")) {
				if (entry.getKey().equals(Misc.STR_TIME + "#hasBeginning")) {
					OWLAxiom durAxiom = df.getOWLDataPropertyAssertionAxiom(
							df.getOWLDataProperty("time:hasDuration", pm), theObject, Misc.calendarToInt(Misc.today()) - entry.getValue());
					newAxioms.add(durAxiom);
				}
			}
		}

		manager.addAxioms(ontology, newAxioms);
		reload();
		if (!reasoner.isConsistent()) {
			manager.removeAxioms(ontology, newAxioms);
			individualMaxNumber--;
			return false;
		}

		keepAllIndividualsDifferent();
		return true;
	}

	public Set<Constraint> getAllConstraints(){
		Set<Constraint> set = new HashSet<Constraint>();
		Map<OWLObjectPropertyExpression,Set<OWLIndividual>> opMap = thePatient.getObjectPropertyValues(ontology);
		for (Map.Entry<OWLObjectPropertyExpression,Set<OWLIndividual>> entry : opMap.entrySet()) {
			String objectSuperType = ((OWLEntity) entry.getKey()).toStringID().replace("has", "");
			for (OWLIndividual object : entry.getValue()) {
				Set<OWLClassExpression> objectTypes = object.getTypes(ontology);
				Map<String,Boolean> boolMods = new HashMap<String,Boolean>();
				for (OWLClassExpression ot : objectTypes) {
					if (ot instanceof OWLClass) {
						boolMods.put(((OWLEntity) ot).toStringID(), true);
					} else if (ot instanceof OWLObjectComplementOf) {
						OWLClassExpression op = ((OWLObjectComplementOf) ot).getOperand();
						if (op instanceof OWLClass) {
							boolMods.put(((OWLEntity) op).toStringID(), false);
						}
					}
				}

				Map<OWLDataPropertyExpression,Set<OWLLiteral>> dpMap = object.getDataPropertyValues(ontology);
				Map<String,Integer> intMods = new HashMap<String,Integer>();
				for (Map.Entry<OWLDataPropertyExpression,Set<OWLLiteral>> dentry : dpMap.entrySet()) {
					for (OWLLiteral literal : dentry.getValue()) {
						if (literal.isInteger()) {
							intMods.put(((OWLEntity) dentry.getKey()).toStringID(), literal.parseInteger());
						}
					}
				}
				boolean isCurrentSession = true;
				Set<OWLIndividual> sessions = object.getObjectPropertyValues(df.getOWLObjectProperty("time:hasSession", pm), ontology);
				for (OWLIndividual s : sessions) {
					// should be exactly one
					isCurrentSession = s.getTypes(ontology).contains(df.getOWLClass("time:CurrentEvent", pm));
					break;
				}
				set.add(new Constraint(objectSuperType, null, object, boolMods, intMods, isCurrentSession));
			}
		}
		return set;
	}

	private void keepAllIndividualsDifferent(){
		Set<OWLNamedIndividual> individuals = ontology.getIndividualsInSignature();
		Set<OWLDifferentIndividualsAxiom> axioms = new HashSet<OWLDifferentIndividualsAxiom>();
		for(OWLNamedIndividual individual: individuals){
			axioms.addAll(ontology.getDifferentIndividualAxioms(individual));
		}
		manager.removeAxioms(ontology,axioms);
		OWLAxiom diffI = df.getOWLDifferentIndividualsAxiom(individuals);
		manager.addAxiom(ontology, diffI);
	}

	public Set<Pair<String,Boolean>> getTypes() {
		Set<OWLClassExpression> types = thePatient.getTypes(ontology);
		Set<Pair<String,Boolean>> typeIRIs = new HashSet<Pair<String,Boolean>>();
		for (OWLClassExpression type : types) {
			if (type instanceof OWLClass) {
				typeIRIs.add(new Pair<String,Boolean>(((OWLEntity) type).toStringID(), true));
			} else if (type instanceof OWLObjectComplementOf) {
				OWLClassExpression subtype = ((OWLObjectComplementOf) type).getOperand();
				if (subtype instanceof OWLClass) {
					typeIRIs.add(new Pair<String,Boolean>(((OWLEntity) subtype).toStringID(), false));
				}
			}
		}
		return typeIRIs;
	}

	public boolean addType(String typeIRI, boolean positive) {
		OWLClassExpression type = df.getOWLClass(IRI.create(typeIRI));
		if (!positive) {
			type = df.getOWLObjectComplementOf(type);
		}
		if (thePatient.getTypes(ontology).contains(type)) {
			return false;
		}
		OWLAxiom ax = df.getOWLClassAssertionAxiom(type, thePatient);
		manager.addAxiom(ontology, ax);
		reload();
		if (!reasoner.isConsistent()) {
			manager.removeAxiom(ontology, ax);
			return false;
		}
		return true;
	}

	public boolean removeType(String typeIRI, boolean positive) {
		OWLClassExpression type = df.getOWLClass(IRI.create(typeIRI));
		if (!positive) {
			type = df.getOWLObjectComplementOf(type);
		}
		if (!thePatient.getTypes(ontology).contains(type)) {
			return false;
		}
		OWLAxiom ax = df.getOWLClassAssertionAxiom(type, thePatient);
		manager.removeAxiom(ontology, ax);
		return true;
	}

}
