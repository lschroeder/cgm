import java.io.File;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.*;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.util.SimpleIRIMapper;

import uk.ac.manchester.cs.owl.owlapi.OWLObjectHasValueImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLLiteralImplBoolean;

//imports for queries
import org.mindswap.pellet.jena.PelletReasonerFactory;

import com.clarkparsia.pellet.sparqldl.jena.SparqlDLExecutionFactory;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;




public class OWLHandler {
	private OWLOntologyManager manager;
	private File ontologyFile;
	private OWLOntology ontology;
	private IRI ontologyIRI;
	private OWLDataFactory df;
	private PrefixManager pm;
	
	private Patient patient;

	private ArrayList<String> symptoms;
	private ArrayList<String> diagnoses;
	private ArrayList<String>  measures;

	private Map<String,ArrayList<String>> classToPreconditionsMap;

	private Set<String> preconditionsSet;

	private Map<String,String> classToAnnotationMap;

	private Map<String,ArrayList<String>> checkMembershipMap;
	//key is the IRIString of the class that is annotated with checkMembership. value is a list of annotation values

	private Map<String,ArrayList<String>> concreteClassToRecommendationMap;

	private Map<String,ArrayList<String>> recommendationClassToGuidelineMap;

	private Map<String,Boolean> preconditionTrivial;

	private Map<String,Boolean> preconditionSatisfiedWithYes;

	private ArrayList<String> patientClasses;


	public OWLHandler(){
		try{
			load(Misc.PATH_SCHIZO);
		}catch(Exception e){
			e.printStackTrace();
		}

		symptoms  = getConcreteClasses("Symptom");
		diagnoses = getConcreteClasses("Diagnosis");
		measures  = getConcreteClasses("Measure");
		
		checkMembershipMap 	= new HashMap<String,ArrayList<String>>();
		classToPreconditionsMap = new HashMap<String,ArrayList<String>>();
		classToAnnotationMap    = new HashMap<String,String>();	
		preconditionsSet 	= new HashSet<String>();
		recommendationClassToGuidelineMap = new HashMap<String,ArrayList<String>>();
		preconditionTrivial	= new HashMap<String,Boolean>();
		preconditionSatisfiedWithYes = new HashMap<String,Boolean>();

		init();
		

		concreteClassToRecommendationMap = getLinkFromClassToRecommendation();


		
	}

	/**
	 * runs once over all classes and saves the connection between classes their
	 *						- checkMembership annotation value
	 *						- precondition annotation value
	 *						- guideline annotation value
	 *						- trivial annotation value
	 *						- rdfs:label annotation value
	 *						- satisfiedWithYes annotation value
	 */
	private void init(){
		Set<OWLClass> classes = ontology.getClassesInSignature();

		for(OWLClass owlclass : classes){
			String className = owlclass.getIRI().toString();
			Set<OWLAnnotation> annotations = owlclass.getAnnotations(ontology);
			for(OWLAnnotation a : annotations){
				if(a.getProperty().getIRI().toString().contains("checkMembership")){
					IRI iri = (IRI) a.getValue();
					if(checkMembershipMap.get(className) == null){
						checkMembershipMap.put(className, new ArrayList<String>());
					}
					checkMembershipMap.get(className).add(iri.toString());
				}else if(a.getProperty().getIRI().toString().contains("precondition")){
					IRI iri = (IRI) a.getValue();
					if(classToPreconditionsMap.get(className) == null){
						classToPreconditionsMap.put(className, new ArrayList<String>());
					}
					classToPreconditionsMap.get(className).add(iri.toString());
					preconditionsSet.add(iri.toString());
				}else if(a.getProperty().isLabel()){
					OWLLiteral literal = (OWLLiteral) a.getValue();
					if(literal.hasLang("en"))
						classToAnnotationMap.put(owlclass.getIRI().toString(),literal.getLiteral());
				}else if(a.getProperty().getIRI().toString().contains("guideline")){
					OWLLiteral literal =  (OWLLiteral) a.getValue();
					String guideline = literal.getLiteral();	
					if(recommendationClassToGuidelineMap.get(className) == null){
						recommendationClassToGuidelineMap.put(className,new ArrayList<String>());
					}
					recommendationClassToGuidelineMap.get(className).add(guideline);
				}else if(a.getProperty().getIRI().toString().contains("trivial")){
					OWLLiteralImplBoolean bool = (OWLLiteralImplBoolean) a.getValue();
					if(bool.getLiteral().equals("true"))
						preconditionTrivial.put(className,true);
					else
						preconditionTrivial.put(className,false);

				}else if(a.getProperty().getIRI().toString().contains("satisfiedWithYes")){
					OWLLiteralImplBoolean bool = (OWLLiteralImplBoolean) a.getValue();
					if(bool.getLiteral().equals("true"))
						preconditionSatisfiedWithYes.put(className,true);
					else
						preconditionSatisfiedWithYes.put(className,false);
					
				}
			}
		}



	}

	/**
	 *	tries to add a new patient and overwrites the actual patient
	 *	adds .owl to the end of the path if necessary
	 * 	@return true if the patient was created
	 * 	         false if the patient already exists or an error occured
	 */
	public boolean addPatient(String path) {
		File file = new File(path);
		String name = file.getName();
		if(name.endsWith(".owl")){
			name = name.substring(0,name.indexOf(".owl"));		
		}else{
			file = new File(path + ".owl");
		}

		try{
			patient = new Patient(name,file,true);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * loads the patient from the given location
	 * @param the path to the ontology of the patient
	 * @return true if successfull, false otherwise
	 */
	public boolean loadPatient(String path){
		File file = new File(path);
		String name = file.getName();

		if(name.endsWith(".owl")){
			name = name.substring(0,name.indexOf(".owl"));		
		}else{
			file = new File(path + ".owl");
		}	

		//System.out.println("try to load patient " + name);
		try{
			patient = new Patient(name,file,false);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * saves the patient
	 */
	public void savePatient(){
		patient.save();
	}

	/**
	 * @return the name of the patient. If no patient is loaded, then null is returned
	 */
	public String getPatientName(){
		if(patient != null){
			return patient.getName();
		}
		return null;
	}

	/**
	 * @return An arraylist containing all concrete subclasses of Symptom 
	 */
	public ArrayList<String> getSymptoms(){
		return symptoms;
	}


	/**
	 * @return An arraylist containing all concrete subclasses of Diagnosis
	 */
	public ArrayList<String> getDiagnoses(){
		return diagnoses;
	}

	/**
	 * @return An arraylist containing all concrete subclasses of Measure 
	 */	
	public ArrayList<String> getMeasures(){
		return measures;	
	}

	/**
	 * @return true if a patient is loaded, false otherwise
	 */
	public boolean patientLoaded(){
		return (patient != null);
	}

	/**
	 * loads the selected schizophrenia (RDF/XML) ontology
	 * the time ontology is still hardcoded
	 */
	private void load(String name) throws OWLOntologyCreationException {
		manager = OWLManager.createOWLOntologyManager();
		ontologyFile = new File(name);
		manager.addIRIMapper(new SimpleIRIMapper(Misc.IRI_TIME,
					IRI.create(Misc.FILE_TIME)));
		ontology = manager.loadOntologyFromOntologyDocument(ontologyFile);
		ontologyIRI = ontology.getOntologyID().getOntologyIRI();
		df = manager.getOWLDataFactory();
		pm = new DefaultPrefixManager(ontologyIRI.toString());
		//System.out.println("Loaded ontology from " + name);
	}


	/**
	 * This method runs over all subclasses (and their subclasses...) of superClassName and searches for classes that are also subclass of Concrete
	 * @param String superClass allowed values: "Measure", "Symptom", "Diagnosis"
	 * @return returns an ArrayList of class names 
	 */
	private ArrayList<String> getConcreteClasses(String superClassName){
		OWLClass superClass = df.getOWLClass("#" + superClassName,pm);

		OWLClass concreteClass = df.getOWLClass("#Concrete",pm);

		Set<OWLClassExpression> concrete = concreteClass.getSubClasses(ontology);

		ArrayList<String> concClasses = new ArrayList<String>();

		Set<OWLClassExpression> subclassesSet = superClass.getSubClasses(ontology);

		Queue<OWLClassExpression> subclasses = new LinkedList<OWLClassExpression>(subclassesSet);

		OWLClassExpression cc;
		while ((cc = subclasses.poll()) != null) {
			OWLClass c;

			if( cc instanceof OWLClass)
				c = (OWLClass) cc;
			else continue;

			for(OWLClassExpression ce : c.getSubClasses(ontology))
				subclasses.add(ce);

			if(concrete.contains(c))
				concClasses.add(c.toStringID());
		}

		return concClasses;
	}


	/** 
	* Searches in the ontology for recommendation classes and the classes they recommend. A recommendation class is detected by being equivalent to a ClassExpression of the form "hasRecommended(Measure/Diagnoses) C", where C is the class that is recommended by the recommendation class. 
	 * @return a map where the key is the IRI string of the class being recommended (f.e. Schizophrenia) and the value is a list of IRI strings of the recommendation class (f.e. RecommendedDiagnosisSchizophrenia). 
	 */
	private Map<String,ArrayList<String>> getLinkFromClassToRecommendation(){
		Map<String,ArrayList<String>> result = new HashMap<String,ArrayList<String>>();

		Set<OWLClass> classes = ontology.getClassesInSignature();

		//System.out.println("Getting link between recommendation and concrete class:");


		for(OWLClass owlclass : classes){

			Set<OWLClassExpression> classExps = owlclass.getSuperClasses(ontology);
			for(OWLClassExpression classExp : classExps){

				if(!(classExp instanceof OWLObjectHasValueImpl))
					continue;
				OWLObjectHasValueImpl hasValue = (OWLObjectHasValueImpl) classExp;

				OWLObjectPropertyExpression propertyExpression = hasValue.getProperty();
				OWLObjectProperty property = propertyExpression.getNamedProperty();

				String iriString = property.getIRI().toString();

				if(!(iriString.contains("hasRecommendedMeasure") || iriString.contains("hasRecommendedDiagnosis")))
					continue;


				OWLIndividual individual = hasValue.getValue();
				Set<OWLClassExpression> types = individual.getTypes(ontology);
				for(OWLClassExpression newClassExp : types){
					if(newClassExp instanceof OWLClass){
						OWLClass newClass = (OWLClass) newClassExp;

						String recommendationClassIRI = owlclass.getIRI().toString();

						//System.out.println(Misc.baseName(newClass.getIRI().toString()) + " " + Misc.baseName(iriString) + " " + Misc.baseName(recommendationClassIRI));

						String classIRI = newClass.getIRI().toString();
						if(result.get(classIRI) == null){
							result.put(classIRI,new ArrayList<String>());
						}
						result.get(classIRI).add(recommendationClassIRI);

					}
				}

			}
		}

		return result;

	}

	/**
	 * Returns the guideline annotations of a recommendation class.
	 * @param the IRI string of an recommendation class.
	 * @return the value of the annotations as ArrayList. The empty list if there is no mapping for the input.
	 */
	public ArrayList<String> getGuidelineAnnotation(String recommendationClass){
		ArrayList<String> list = recommendationClassToGuidelineMap.get(recommendationClass);
		if (list == null ){
			return new ArrayList<String>();
		}else{
			return list;
		}

	}

	/** 
	 * Returns the recommendation class for the given class if the patient is a member of it. For example IRI + "Schizophrenia" maps to IRI + "RecommendDiagnosisSchizophrenia".
	 * @param the IRI string of the recommended class
	 * @return a ArrayList of IRI strings of the recommendation classes the patient satisfies. An empty list if no mapping exists for the input.
	 */
	public ArrayList<String> getRecommendationClass(String concreteClass){
		ArrayList<String> list = concreteClassToRecommendationMap.get(concreteClass);

		
		ArrayList<String> result = new ArrayList<String>();

		if(list == null) return null;

		if(patientClasses == null)
			queryForPatientClasses();

		for(String s : list){
			if(patientClasses.contains(s))
				result.add(s);
		}
		return result;
	}

	/**
	 * returns a list of IRI Strings. There are the classes being connected to the owlClass via the checkMembership annotation
	 * @param owlclass: the IRI String of the class
	 * @return an ArrayList of IRI Strings
	 * 
	 */
	public ArrayList<String> getCheckMembershipClasses(String owlClass){
		ArrayList<String> list = checkMembershipMap.get(owlClass);
		return list;
		//return (list == null) ? new ArrayList<String>() : list;
	}

	/**
	 * prepares a query on the patient ontoloy
	 * The patient ontology is loaded from file, so saving the ontology may be necessary before calling this method.
	 * @return the QueryExecution object qe. queries can now be executed by qe.execSelect() or qe.execAsk(). More details here: http://jena.apache.org/documentation/javadoc/arq/com/hp/hpl/jena/query/QueryExecution.html
	 */
	private QueryExecution query(Query query){

		//http://answers.semanticweb.com/questions/12147/whats-the-best-way-to-parameterize-sparql-queries

		OntDocumentManager mgr = new OntDocumentManager();

		//imports
		mgr.addAltEntry(Misc.STR_TIME,Misc.PATH_TIME);
		mgr.addAltEntry(Misc.STR_SCHIZO,Misc.PATH_SCHIZO);

		OntModelSpec s = new OntModelSpec(PelletReasonerFactory.THE_SPEC);

		s.setDocumentManager(mgr);	

		OntModel ontModel = ModelFactory.createOntologyModel(s);
		try{ 
			ontModel.read(patient.getFile().toURI().toString());
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}

		QueryExecution qe = SparqlDLExecutionFactory.create( query, ontModel );

		//ResultSet rs = qe.execSelect();
		//ResultSetFormatter.out(rs);

		return qe;
	}

	/**
	 * Gets all Constraints of the actual patient. Therefore calls getAllContraints in the patient class and adds the objectLabel and the objectType attribute to the constraints.
	 * @return ArrayList of constraints
	 */
	public ArrayList<Constraint> getAllConstraints(){

		if (patient == null) {
			return new ArrayList<Constraint>();
		}

		ArrayList<Constraint> constraints = new ArrayList<Constraint>(patient.getAllConstraints());

		Iterator<Constraint> iter = constraints.iterator();

		while(iter.hasNext()){
			Constraint constraint = iter.next();

			Map<String,Boolean> boolMods = constraint.getBoolMods();
			Set<String> boolModKeySet = boolMods.keySet();

			String key;
			Iterator<String> iterKeySet = boolModKeySet.iterator();

			while(iterKeySet.hasNext()){

				key = iterKeySet.next();

				String s;

				if((symptoms.contains(key)) || (diagnoses.contains(key)) || (measures.contains(key))){
					constraint.setObjectType(key);
					constraint.setObjectLabel(getLabel(key));
					boolMods.remove(key);
					constraint.setBoolMods(boolMods);
					break;
				}	
			}
		}

		return constraints;	
	}


	/**
	 * Adds a constraint to the actual patient
	 * @param constraint: constraint having at least values for objectSuperType and objectType
	 * @return true if successfull, false otherwise
	 */
	public boolean addConstraint(Constraint constraint) throws Exception{
		return patient.addConstraint(constraint);

	}

	/**
	 * Removes a constraint of the actual patient 
	 * @param constraint: a constraint in the format like returned from getAllConstraints()
	 * @return true if successfull, false otherwise
	 */
	public boolean removeConstraint(Constraint constraint) {
		return patient.removeConstraint(constraint);
	}

	public boolean endConstraint(Constraint constraint, int endDate) {
		return patient.endConstraint(constraint, endDate);
	}

	public boolean endSession() {
		return patient.endSession();
	}


	/**
	 * Queries for all classes the actual patient belongs to
	 * @return an ArrayList of classNames
	 */
	private ArrayList<String> queryForPatientClasses(){

		String patientIRI = patient.getIRIString(); 

		ParameterizedSparqlString queryStr = new ParameterizedSparqlString();
		queryStr.setNsPrefix("schizophrenia", Misc.STR_SCHIZO + "#");
		queryStr.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		queryStr.append("SELECT ?class WHERE{");
		queryStr.append("<" + patientIRI + "#" + patient.getName() + ">" + " a ?class.");
		queryStr.append("}");
		Query query = queryStr.asQuery();
		QueryExecution qe = query(query);
		ResultSet rs = qe.execSelect();


		ArrayList<String> classes = new ArrayList<String>();

		//System.out.println("Patient " + patient.getName() + " is in the following precondition classes:" );

		while(rs.hasNext()){
			QuerySolution qs = rs.next();
			String className = qs.getResource("class").toString();
			classes.add(className);
			//System.out.println(className);


		}

		qe.close();

		patientClasses = classes;

		return classes;	

	}

	/**
	 * Checks whether the patient is in the complement of a class
	 * @param preconditionName name of the class
	 * @return true if the patient is in the complement, false otherwise 
	 */
	private boolean queryPatientInComplementOf(String preconditionName){
		String patientIRI = patient.getIRIString(); 

		ParameterizedSparqlString queryStr = new ParameterizedSparqlString();
		queryStr.setNsPrefix("schizophrenia", Misc.STR_SCHIZO + "#");
		queryStr.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		queryStr.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
		queryStr.append("ASK {");
		queryStr.append("<" + patientIRI + "#" + patient.getName() + ">" + " a [owl:complementOf schizophrenia:" + Misc.baseName(preconditionName) + " ]. }");

		Query query = queryStr.asQuery();

		QueryExecution qe = query(query);

		boolean answer = qe.execAsk();
		qe.close();

		return answer;

	}

	/**
	 * Checks for all classes the patient belongs to, whether he is in the correspondig precondition class, he is not or we simply do not know it.
	 *
	 * @return returns a map, whichs key is the name of the recommendation class. The value is a List of String arrays with two entries. The first is the the name the preconditon class, the second is the status. Status can be "confirmed", "excluded" or "unknown".
	 *
	 */
	public Map<String,ArrayList<String[]>> queryForPatientInPreconditionClass(){

		//maps from preconditionClassName to (OriginalClass,Status)
		Map<String,ArrayList<String[]>> resultMap = new HashMap<String,ArrayList<String[]>>();

		ArrayList<String> patientClasses = queryForPatientClasses();

		//System.out.println("Checking preconditions:");

		for(String className : patientClasses){

			List<String> preconditions = classToPreconditionsMap.get(className);

			if(preconditions == null) continue; //no class with preconditions

			for(String preconditionName : preconditions){
				String[] res = new String[2];
				res[0] = preconditionName;

				if(patientClasses.contains(preconditionName)){
					res[1] = "yes";
				}else if(queryPatientInComplementOf(preconditionName)){
					res[1] = "no";
				}else{
					res[1] = "unknown";
				}

				if(resultMap.get(className) == null)
					resultMap.put(className,new ArrayList<String[]>());

				resultMap.get(className).add(res);

				//System.out.println(className + " " + res[0] + " " + res[1]);
			}	
		}

		return resultMap;
	}


	/**
	 * Queries for recommendedDiagnosis or recommendedMeasure, depending on the input
	 * @param decide: "Measure" for recommendedMeasure, "Diagnosis" for recommendedDiagnosis. Null is returned for any other input.
	 * @return an ArrayList containing the recommended classes f.e. IRI + "Schizophrenia"
	 */
	private ArrayList<String> queryForRecommended(String decide){
		String patientIRI = patient.getIRIString();
		String patientQuery = "<" + patientIRI + "#" + patient.getName() + ">";
		ParameterizedSparqlString queryStr = new ParameterizedSparqlString();
		queryStr.setNsPrefix("schizophrenia", Misc.STR_SCHIZO + "#");
		queryStr.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		queryStr.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		queryStr.append("SELECT ?diag WHERE{");
		if(decide.equals("Diagnosis")){
			queryStr.append(patientQuery + " schizophrenia:hasRecommendedDiagnosis ?diag.");
			queryStr.append("FILTER NOT EXISTS { " +  patientQuery + " schizophrenia:hasNonRecommendedDiagnosis ?diag}.");
		}else if(decide.equals("Measure")){
			queryStr.append(patientQuery + " schizophrenia:hasRecommendedMeasure ?diag.");
			queryStr.append("FILTER NOT EXISTS { " +  patientQuery + " schizophrenia:hasNonRecommendedMeasure ?diag}.");
		}else{
			return null;
		}
		queryStr.append("}");
		Query query = queryStr.asQuery();
		QueryExecution qe = query(query);
		ResultSet rs = qe.execSelect();

		ArrayList<String> result = new ArrayList<String>();

		while(rs.hasNext()){
			QuerySolution qs = rs.next();

			Individual in = qs.getResource("diag").as(Individual.class);
			ExtendedIterator it = in.listOntClasses(true);

			String className = "";
			while(it.hasNext()){
				className = it.next().toString();
			}
			result.add(className);
			//System.out.println("Recommended " + decide + ": " + className);	
		}

		qe.close();
		return result;	

	}

	/**
	 * Queries for recommendedDiagnosis of the current patient
	 * @return an ArrayList containing the recommended diagnoses f.e. IRI + "Schizophrenia"
	 */
	public ArrayList<String> queryForRecommendedDiagnoses(){
		return queryForRecommended("Diagnosis");
	}

	/**
	 * Queries for recommendedDiagnosis of the current patient
	 * @return an ArrayList containing the recommended measures 
	 */
	public ArrayList<String> queryForRecommendedMeasures(){
		return queryForRecommended("Measure");	
	}

	/**
	 * Returns all precondition classes the patient explicitly belongs to. That means only precondition classes that were added using addType()
	 * @return an ArrayList of the IRI strings of precondition classes
	 */
	public ArrayList<Pair<String,Boolean>> getExplicitPreconditions(){
		Set<Pair<String,Boolean>> types = patient.getTypes();

		ArrayList<Pair<String,Boolean>> preconditions = new ArrayList<Pair<String,Boolean>>();

		for(Pair<String,Boolean> s : types){
			if(!preconditionsSet.contains(s.fst))
				continue;
			preconditions.add(s);
		}

		return preconditions;
	}

	public boolean addType(String typeIRI, boolean positive){
		return patient.addType(typeIRI, positive);
	}

	public boolean removeType(String typeIRI, boolean positive){
		return patient.removeType(typeIRI, positive);
	}

	/**
	 * @param className: the IRI string of a class
	 * @return the rdfs:label annotation of the class. if no annotation exists, then the name of the class is returned.
	 */
	public String getLabel(String className){
		String annotation = classToAnnotationMap.get(className);
		return (annotation == null) ? Misc.baseName(className) : annotation;
	}

	/**
	 * @param preconditionClassName: the IRI string of a class
	 * @return true, if the precondition is trivial
	 */
	public boolean isTrivial(String preconditionClassName) {

		return preconditionTrivial.get(preconditionClassName);
	}

	public boolean preconditionSatisfiedWithYes(String preconditionClassName){
		return preconditionSatisfiedWithYes.get(preconditionClassName);
	}




}
