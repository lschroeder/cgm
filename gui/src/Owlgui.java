
import java.text.DecimalFormat;
import java.util.*;
import java.util.Map.Entry;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.widgets.List;

public class Owlgui {

	private static OWLHandler handler;
	private static Display display;
	private static Shell shell;
	private static ArrayList<Constraint> currentconstraints;
	private static ArrayList<Constraint> pastconstraints;
	private static ArrayList<String> classes;
	private static Map<String, String> maplabel;
	private static Map<String, String> checkMembershipLabels;
	private static Map<String, ArrayList<String[]>> checkPreconditions;
	private static ArrayList<String> recommendation;
	private static ArrayList<Pair<String,Boolean>> preconditions;

	public static void main(String[] args) {
		checkMembershipLabels = new HashMap<String, String>();
		display = new Display();

		shell = new Shell(display);
		GridLayout layout = new GridLayout();
		layout.numColumns = 5;
		shell.setLayout(layout);

		shell.setMinimumSize(600, 800);
		shell.setText("CGM/F20");
		Monitor primary = display.getPrimaryMonitor();
		org.eclipse.swt.graphics.Rectangle bounds = primary.getBounds();
		org.eclipse.swt.graphics.Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);

                final Image image = new Image(display,"bigdoctor.png");
                final Shell splash = new Shell(SWT.ON_TOP);
                Label flabel = new Label(splash, SWT.NONE);
                flabel.setImage(image);
                FormLayout flayout = new FormLayout();
                splash.setLayout(flayout);
                splash.pack();
                splash.setLocation(x,y);
                splash.open();

		Menu menuBar = new Menu(shell, SWT.BAR);
		MenuItem fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuHeader.setText("Patient");
		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuHeader.setMenu(fileMenu);
		MenuItem fileNewItem = new MenuItem(fileMenu, SWT.PUSH);
		fileNewItem.setText("New patient");
		MenuItem fileLoadItem = new MenuItem(fileMenu, SWT.PUSH);
		fileLoadItem.setText("Load patient");
		MenuItem fileSaveItem = new MenuItem(fileMenu, SWT.PUSH);
		fileSaveItem.setText("Save patient");
		shell.setMenuBar(menuBar);

		final TabFolder tabFolder = new TabFolder(shell, SWT.BORDER);
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true,
				true);
		gridData.horizontalSpan = 5;
		tabFolder.setLayoutData(gridData);

		TabItem recentTab = new TabItem(tabFolder, SWT.PUSH);
		recentTab.setText("Recent history");
		TabItem pastTab = new TabItem(tabFolder, SWT.PUSH);
		pastTab.setText("Past history");
		TabItem questionTab = new TabItem(tabFolder, SWT.PUSH);
		questionTab.setText("Guideline assistant");

		
		//Guideline Assistant Tab Content
		
		final Composite compQuery = new Composite(tabFolder, SWT.NONE);
		GridLayout compLayout = new GridLayout();
		compLayout.numColumns = 1;
		final Table rdTable = new Table(compQuery, SWT.BORDER);
		GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true,
				true);
		gridData2.horizontalSpan = 1;
		rdTable.setLayoutData(gridData2);
		rdTable.setLinesVisible (true);
		rdTable.setHeaderVisible (true);
		final Button showInfoButton = new Button(compQuery, SWT.PUSH);
		showInfoButton.setText("Show missing info");
		showInfoButton.setEnabled(false);
		TableColumn column1 = new TableColumn (rdTable, SWT.NONE);
		TableColumn column2 = new TableColumn (rdTable, SWT.NONE);
		TableColumn column3 = new TableColumn (rdTable, SWT.NONE);
		column1.setText("Matched recommendation");
		column2.setText("Status");
		column3.setText("Guideline sections");
		compQuery.setLayout(compLayout);
		questionTab.setControl(compQuery);		
		
		//Recent Tab Content
		
		final Composite compRecent = new Composite(tabFolder, SWT.NONE);
		compLayout = new GridLayout();
		compLayout.numColumns = 7;
		compRecent.setLayout(compLayout);
		final Table recentTable = new Table(compRecent, SWT.BORDER | SWT.V_SCROLL
				| SWT.SINGLE);
		GridData gridData6 = new GridData(GridData.FILL, GridData.FILL, true,
				true);
		gridData6.horizontalSpan = 7;
		GridData gridDatarow = new GridData(SWT.BEGINNING, SWT.END, false,
				false);
		gridDatarow.horizontalSpan = 7;
		recentTable.setLayoutData(gridData6);
		recentTable.setLinesVisible(true);
		recentTable.setHeaderVisible(true);
		TableColumn columnRecent1 = new TableColumn (recentTable, SWT.NONE);
		TableColumn columnRecent2 = new TableColumn (recentTable, SWT.NONE);
		TableColumn columnRecent3 = new TableColumn (recentTable, SWT.NONE);
		TableColumn columnRecent4 = new TableColumn (recentTable, SWT.NONE);
		columnRecent1.setText("Event");
		columnRecent2.setText("Type");
		columnRecent3.setText("Start date");
		columnRecent4.setText("End date");
		final Button addSymptomButton = new Button(compRecent, SWT.PUSH);
		addSymptomButton.setText("Record symptom");
		final Button addDiagnosisButton = new Button(compRecent, SWT.PUSH);
		addDiagnosisButton.setText("Record diagnosis");
		final Button addMeasureButton = new Button(compRecent, SWT.PUSH);
		addMeasureButton.setText("Record measure");
		final Button addSuspectedDiagnosisButton = new Button(compRecent, SWT.PUSH);
		addSuspectedDiagnosisButton.setText("Add suspected diagnosis");
		final Button editConstraintButton = new Button(compRecent, SWT.PUSH);
		editConstraintButton.setText("Edit selection");
		final Button removeConstraintButton = new Button(compRecent, SWT.PUSH);
		removeConstraintButton.setText("Remove selection");
		final Button endSessionButton = new Button(compRecent, SWT.PUSH);
		endSessionButton.setText("Release patient");
		Label preconditionsLabel = new Label(compRecent, SWT.NONE);
		preconditionsLabel.setLayoutData(null);
		preconditionsLabel.setText("Additional information:");
		final Table preconditionsTable = new Table(compRecent, SWT.BORDER | SWT.V_SCROLL | SWT.SINGLE);
		preconditionsTable.setLayoutData(gridData6);
		preconditionsTable.setLinesVisible(true);
		preconditionsTable.setHeaderVisible(true);
		TableColumn columnPreconditions1 = new TableColumn (preconditionsTable, SWT.NONE);
		TableColumn columnPreconditions2 = new TableColumn (preconditionsTable, SWT.NONE);
		columnPreconditions1.setText("Question");
		columnPreconditions2.setText("Answer");
		final Button removePreconditionButton = new Button(compRecent, SWT.PUSH);
		removePreconditionButton.setText("Remove selection");
		removePreconditionButton.setLayoutData(gridDatarow);
		recentTab.setControl(compRecent);

		//Past Session/History Tab Content

		final Composite compPast = new Composite(tabFolder, SWT.NONE);
		compPast.setLayout(compLayout);
		final Table pastTable = new Table(compPast, SWT.BORDER | SWT.V_SCROLL
				| SWT.SINGLE);
		pastTable.setLayoutData(gridData6);
		pastTable.setLinesVisible(true);
		pastTable.setHeaderVisible(true);
		TableColumn columnPast1 = new TableColumn (pastTable, SWT.NONE);
		TableColumn columnPast2 = new TableColumn (pastTable, SWT.NONE);
		TableColumn columnPast3 = new TableColumn (pastTable, SWT.NONE);
		TableColumn columnPast4 = new TableColumn (pastTable, SWT.NONE);
		columnPast1.setText("Event");
		columnPast2.setText("Type");
		columnPast3.setText("Start date");
		columnPast4.setText("End date");
		final Button endConstraintButton = new Button(compPast, SWT.PUSH);
		endConstraintButton.setText("Set end date");
		pastTab.setControl(compPast);

		final Label label = new Label(shell, SWT.BORDER | SWT.BOTTOM);
		label.setText("No patient loaded");
		gridData = new GridData(GridData.FILL, GridData.END, false, false);
		gridData.horizontalSpan = 5;
		label.setLayoutData(gridData);
		
		// remove Preconditions
		removePreconditionButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = preconditionsTable.getSelectionIndex();
				if(i == -1)
					return;
				else{
					handler.removeType(preconditions.get(i).fst, preconditions.get(i).snd);
					fillPreconditionsTable(preconditionsTable);
				}
			}
		});
		
		//activate button in recommendation tab
		rdTable.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				showInfoButton.setEnabled(true);
			}
		});
		
		/*
		 * tab change event handler
		 */
		tabFolder.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (tabFolder.getSelectionIndex() == 1) {
					showInfoButton.setEnabled(false);
					if (handler.patientLoaded()){
						fillHistoryTable(pastTable, false);
					}
				} else if (tabFolder.getSelectionIndex() == 2) {
					if (handler.patientLoaded()) {
						/*MessageBox mb = new MessageBox(shell, SWT.OK
								| SWT.CANCEL);
						mb.setText("Warning");
						mb.setMessage("Patient will be saved for Query");*/
						int i = SWT.OK; // mb.open();
						if (i == SWT.OK) {
							handler.savePatient();
							fillRecommendationsTable(rdTable);
						} else
							tabFolder.setSelection(0);
					}
				} else if (tabFolder.getSelectionIndex() == 0 && handler.patientLoaded()){
					showInfoButton.setEnabled(false);
					fillHistoryTable(recentTable, true);
					fillPreconditionsTable(preconditionsTable);
				}
			}
		});
		/*
		 * Create a new patient
		 */
		fileNewItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					String file;
					FileDialog filemenu = new FileDialog(shell, SWT.SAVE);
					filemenu.setText("Patient Location");
					file = filemenu.open();
					if (file == null)
						return;
					if (!handler.addPatient(file)) {
						error("Did not create patient");
					} else if (handler.patientLoaded()) {
						label.setText("Current patient: "
								+ handler.getPatientName());
						tabFolder.setSelection(0);
						fillHistoryTable(recentTable, true);
						fillPreconditionsTable(preconditionsTable);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
					error("Exception: " + e1.getMessage());
				}
			}
		});
		/*
		 * Open Calendar to give the constraint an end date
		 */
		endConstraintButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = tabFolder.getSelectionIndex() == 0 ? recentTable
						.getSelectionIndex() : pastTable.getSelectionIndex();
				if (i == -1) {
					System.out.println("No constraint selected");
					return;
				}
				Constraint edit = pastconstraints.get(i);
				dateMenu("end date", edit);
				fillHistoryTable(pastTable, false);
			}
		});
		/*
		 * create new constraint and remove old one (edit button)
		 */
		editConstraintButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = tabFolder.getSelectionIndex() == 0 ? recentTable
						.getSelectionIndex() : pastTable.getSelectionIndex();
				if (i == -1) {
					System.out.println("No constraint selected");
					return;
				}
				Constraint edit = currentconstraints.get(i);
				constraintMenu(edit);
				fillHistoryTable(recentTable, true);
			}
		});
		/*
		 * delete selected constraint
		 */
		removeConstraintButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = tabFolder.getSelectionIndex() == 0 ? recentTable
						.getSelectionIndex() : pastTable.getSelectionIndex();
				if (i == -1) {
					System.out.println("No constraint selected");
					return;
				}
				Constraint edit = (Constraint) currentconstraints.get(i);
				handler.removeConstraint(edit);
				fillHistoryTable(recentTable, true);
			}
		});
		/*
		 * load patient
		 */
		fileLoadItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					if (loadMenu()) {
						label.setText("Current patient: "
								+ handler.getPatientName());
						//label.pack();
						tabFolder.setSelection(0);
						fillHistoryTable(recentTable, true);
						fillPreconditionsTable(preconditionsTable);
						shell.pack();
					}
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		/*
		 * save patient
		 */
		fileSaveItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					if (handler.patientLoaded())
						handler.savePatient();
					tabFolder.setSelection(0);
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		/*
		 * add symptom
		 */
		addSymptomButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					if (!handler.patientLoaded())
						error("No patient selected.");
					else {
						if (!constraintMenu(new Constraint(Misc.STR_SCHIZO
								+ "#Symptom", null, null,
								new HashMap<String, Boolean>(),
								new HashMap<String, Integer>(), true))) {
							error("Could not add symptom.");
						}
						fillHistoryTable(recentTable, true);
					}
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		/*
		 * add diagnosis
		 */
		addDiagnosisButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					if (!handler.patientLoaded())
						error("No patient selected.");
					else {
						if (!constraintMenu(new Constraint(Misc.STR_SCHIZO
								+ "#Diagnosis", null, null,
								new HashMap<String, Boolean>(),
								new HashMap<String, Integer>(), true))) {
							error("Could not add diagnosis.");
						}
						fillHistoryTable(recentTable, true);
					}
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		/*
		 * add measure
		 */
		addMeasureButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					if (!handler.patientLoaded())
						error("No patient selected.");
					else {
						if (!constraintMenu(new Constraint(Misc.STR_SCHIZO
								+ "#Measure", null, null,
								new HashMap<String, Boolean>(),
								new HashMap<String, Integer>(), true))) {
							error("Could not add measure.");
						}
						fillHistoryTable(recentTable, true);
					}
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		/*
		 * add suspected diagnosis
		 */
		addSuspectedDiagnosisButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					if (!handler.patientLoaded())
						error("No patient selected.");
					else {
						if (!constraintMenu(new Constraint(Misc.STR_SCHIZO
								+ "#SuspectedDiagnosis", null, null,
								new HashMap<String, Boolean>(),
								new HashMap<String, Integer>(), true))) {
							error("Could not add suspected diagnosis.");
						}
						fillHistoryTable(recentTable, true);
					}
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		
		//end the current session
		
		endSessionButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					if (!handler.patientLoaded())
						error("No patient selected.");
					else {
						if (!handler.endSession()) {
							error("Could not end session.");
						}
						fillHistoryTable(recentTable, true);
					}
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		
		//show preconditions, opens precondition menue
		
		showInfoButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = rdTable.getSelectionIndex();
				if (i == -1) {
					System.out.println("No recommendation selected");
					return;
				}
				String rec = recommendation.get(i);
				preconditionMenu(rec);
				handler.savePatient();				
				fillRecommendationsTable(rdTable);
				showInfoButton.setEnabled(false);
			}
		});

		// awesome owl
		display.asyncExec(new Runnable() {
			public void run() {
				handler = new OWLHandler();
				splash.close();
				image.dispose();
				shell.pack();
				shell.open();
			}   
		}); 
		while (!shell.isDisposed()) {
			if (!shell.isDisposed()) {
				if (!display.readAndDispatch())
					display.sleep();
			}
		}
		display.dispose();
	}
	
	/**
	 * Fills the precondition/additional information list
	 * @param list The list to fill in
	 */
	private static void fillPreconditionsTable(Table table){
		preconditions = handler.getExplicitPreconditions();
		table.removeAll();
		for (Pair<String,Boolean> k: preconditions) {
			TableItem item = new TableItem(table, SWT.NONE);
			boolean sY = handler.preconditionSatisfiedWithYes(k.fst);
			item.setText(0, handler.getLabel(k.fst));
			item.setText(1, k.snd && sY || !k.snd && !sY ? "yes" : "no"); 	
		}
		table.getColumn(0).pack();
		table.getColumn(1).pack();
		table.getColumn(0).setWidth(500);
	}
	/**
	 * Opens a file dialog to chose a patient and loads the selected patient
	 */
	private static boolean loadMenu() throws Exception {
		String file;
		FileDialog filemenu = new FileDialog(shell, SWT.OPEN);
		filemenu.setText("Load patient");
		file = filemenu.open();
		if (file == null) {
			return false;
		}
		if (handler.loadPatient(file))
			return true;
		return false;
	}

	/**
	 * prints error message
	 * @param m The message to print
	 */
	private static void error(String m) {
		MessageBox mb = new MessageBox(shell);
		mb.setText("Error");
		mb.setMessage(m);
		mb.open();
	}

	/**
	 * Fill a table with constraints
	 * @param table The table to fill
	 * @param recent Fills either only past session events (recent == false) or only current session events (recent == true)
	 */
	private static void fillHistoryTable(Table table, boolean recent) {
		ArrayList<Constraint> constraints = handler.getAllConstraints();
		Collections.sort(constraints);
		currentconstraints = new ArrayList<Constraint>();
		pastconstraints = new ArrayList<Constraint>();
		table.removeAll();
		for (Constraint s : constraints) {
			if (s.isCurrentSession()) {
				currentconstraints.add(s);
			} else {
				pastconstraints.add(s);
			}
			if (s.isCurrentSession() == recent) {
				int begin = s.getBegin();
				int end = s.getEnd();
				DecimalFormat mFormat = new DecimalFormat("00");
				Calendar calBegin = Misc.intToCalendar(begin);
				Calendar calEnd = Misc.intToCalendar(end);
				String beginDate = mFormat.format(calBegin.get(Calendar.DAY_OF_MONTH)) + "." +
					mFormat.format(calBegin.get(Calendar.MONTH) + 1) + "." +
					calBegin.get(Calendar.YEAR);
				String endDate = end == -1 ? "" : mFormat.format(calEnd.get(Calendar.DAY_OF_MONTH))+ "."+
						mFormat.format(calEnd.get(Calendar.MONTH) + 1) + "." +
					calEnd.get(Calendar.YEAR);
				TableItem item = new TableItem(table, SWT.NONE);

				String event = Misc.baseName(s.getObjectSuperType());
				if(event.equals("SuspectedDiagnosis"))
					event = "Suspected diagnosis";

				item.setText(0, event);
				
				
				item.setText(1, s.getObjectLabel());
				item.setText(2, beginDate);
				item.setText(3, endDate);
			}
		}
		table.getColumn(0).pack();
		table.getColumn(1).pack();
		table.getColumn(2).pack();
		table.getColumn(3).pack();
		table.getColumn(0).setWidth(150);
		table.getColumn(1).setWidth(300);
		table.getColumn(2).setWidth(110);
		table.getColumn(3).setWidth(110);
	}

	/**
	 * Fills the recommendation table for diagnoses and measures
	 * @param table The table to fill
	 */
	private static void fillRecommendationsTable(Table table) {
		table.removeAll();
		recommendation = handler.queryForRecommendedDiagnoses();
		ArrayList<String> list = recommendation;
		checkPreconditions = handler.queryForPatientInPreconditionClass();
		for(String rec : list){
			fillRecTableHelper(rec, table, "Diagnosis");
		}
		list = handler.queryForRecommendedMeasures();
		recommendation.addAll(list);
		for(String rec : list){
			fillRecTableHelper(rec, table, "Measure");
		}
		table.getColumn(0).pack();
		table.getColumn(1).pack();
		table.getColumn(2).pack();
		table.getColumn(0).setWidth(500);
		table.getColumn(1).setWidth(300);
		table.getColumn(2).setWidth(100);
	}
	
	/**
	 * Adds an item with the recommendation to the given Table
	 * @param rec The recommendation class e.g. Schizophrenia
	 * @param table The table
	 */
	private static void fillRecTableHelper(String rec, Table table, String type){
		ArrayList<String> recClasses = handler.getRecommendationClass(rec);
		ArrayList<String> toRemove = new ArrayList<String>();
		TableItem item;
		boolean sY;
		boolean foundKnown = false;
		String guideline = "";
		
		for(String recClass : recClasses){
			boolean foundUnknown = false;
			if(checkPreconditions.get(recClass) != null){
				for (String[] s : checkPreconditions.get(recClass)) {
					sY = handler.preconditionSatisfiedWithYes(s[0]);
					if (s[1].equals("no") && !sY || s[1].equals("yes") && sY) {
						toRemove.add(recClass);
						break;
					}
					if (s[1].equals("unknown")) {
						foundUnknown = true;
					}
				}
			}
			if(!foundUnknown){ 
				foundKnown = true;
				ArrayList<String> annotations = handler.getGuidelineAnnotation(recClass);
				for(String s : annotations)
					guideline += s + ", ";
			}
		}
		for(String c : toRemove)
			recClasses.remove(c);
		if(!foundKnown){
			for(String recClass : recClasses){
				ArrayList<String> list = handler.getGuidelineAnnotation(recClass);
				for(String a : list){
					guideline += a + ", ";
				}
			}
		}
		if(!recClasses.isEmpty()){
			item = new TableItem(table, SWT.NONE);
			item.setText(0, type + ": " + handler.getLabel(rec));
			if(!foundKnown){
				item.setText(1, "Possible (missing information)");
			}
			else{
				item.setText(1, "Recommended");
			}
			guideline = guideline.substring(0, guideline.length()-2);
			item.setText(2, guideline);
		}
	}


	/**
	 * Menu to choose an end date, changes nothing if end date already exists
	 * @param edit Constraint that should get an end date
	 * @param m Message on top of new menu
	 */
private static void dateMenu(String m, final Constraint edit){
		final Shell newshell = new Shell(display, SWT.SHEET);
		GridLayout layout = new GridLayout();
		newshell.setLayout(layout);
		newshell.setMinimumSize(500, 300);
		newshell.setText(m);
		Monitor primary = display.getPrimaryMonitor();
		org.eclipse.swt.graphics.Rectangle bounds = primary.getBounds();
		org.eclipse.swt.graphics.Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		newshell.setLocation(x, y);
		
		//Two Buttons and a dateTime widget
		final DateTime date = new DateTime(newshell, SWT.CALENDAR);
		Button okButton = new Button(newshell, SWT.PUSH);
		Button cancelButton = new Button(newshell, SWT.PUSH);
		okButton.setText("OK");
		cancelButton.setText("Cancel");
		
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				newshell.dispose();
			}
		});
		okButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Calendar c = new GregorianCalendar(date.getYear(), date.getMonth(), date.getDay());
				handler.endConstraint(edit, Misc.calendarToInt(c));
				newshell.dispose();
			}
		});
		
		//open shell
				newshell.pack();
				newshell.open();
				while (!newshell.isDisposed()) {
					if (!newshell.isDisposed()) {
						if (!display.readAndDispatch())
							display.sleep();
					}
				}
				
	}
	
	/**
	 * Adds a constraint in the current session to the current patient
	 * @param constraint Decides the default setting of the menu
	 */
	private static boolean constraintMenu(final Constraint c) {
		final Shell newshell = new Shell(display, SWT.SHEET);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		newshell.setLayout(layout);
		newshell.setMinimumSize(200, 200);
		Monitor primary = display.getPrimaryMonitor();
		org.eclipse.swt.graphics.Rectangle bounds = primary.getBounds();
		org.eclipse.swt.graphics.Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		newshell.setLocation(x, y);

		final Combo comboObject = new Combo(newshell, SWT.DROP_DOWN
				| SWT.READ_ONLY);
		GridData gridData3 = new GridData(GridData.FILL, GridData.FILL, true,
				true);
		gridData3.horizontalSpan = 3;
		GridData gridData2 = new GridData();
		gridData2.horizontalSpan = 2;
		GridData gridData31 = new GridData(GridData.BEGINNING, GridData.BEGINNING, false,
				false);
		gridData31.horizontalSpan = 3;
		comboObject.setLayoutData(gridData31);
		
		final Label timelabel = new Label(newshell, SWT.READ_ONLY);
		timelabel.setText("Start date:");
		final DateTime btime = new DateTime(newshell, SWT.CALENDAR);
		final Button completedBox = new Button(newshell, SWT.CHECK);
		completedBox.setText("End date:");
		final DateTime etime = new DateTime(newshell, SWT.CALENDAR);
		etime.setEnabled(false);
		timelabel.setLayoutData(gridData2);
		completedBox.setLayoutData(gridData2);

		final Table checkList = new Table(newshell, SWT.CHECK | SWT.V_SCROLL);
		checkList.setLayoutData(gridData3);
		checkList.setVisible(false);
		checkList.setLinesVisible (true);
		checkList.setHeaderVisible (true);
		TableColumn column1 = new TableColumn (checkList, SWT.NONE);
		column1.setText("Symptom description");
		
		final Button saveButton = new Button(newshell, SWT.PUSH);
		saveButton.setText("OK");
		final Button cancelButton = new Button(newshell, SWT.PUSH);
		cancelButton.setText("Cancel");

		String ontSuperClass = c.getObjectSuperType();
		if (ontSuperClass.equals(Misc.STR_SCHIZO + "#Measure")) {
			classes = handler.getMeasures();
			newshell.setText("Record measure");
		} else if (ontSuperClass.equals(Misc.STR_SCHIZO + "#Symptom")) {
			classes = handler.getSymptoms();
			newshell.setText("Record symptom");
		} else {
			classes = handler.getDiagnoses();
			newshell.setText("Record diagnosis");
		}
		Collections.sort(classes);
		maplabel = new HashMap<String,String>();
		for (String s : classes) {
			comboObject.add(handler.getLabel(s));
			maplabel.put(handler.getLabel(s), s);
		}
		comboObject.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				newshell.pack();
			}
		});
		completedBox.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if(completedBox.getSelection())
				etime.setEnabled(true);
				else
					etime.setEnabled(false);
			}
		});
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				newshell.dispose();
			}
		});
		saveButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Constraint constraint;
				String type = c.getObjectSuperType();
				String concrete = maplabel.get(comboObject.getText());
				if(concrete == null){
					error("Please select type");
					return;
				}
				boolean noerror = true, donothing = false;
				Map<String,Integer> intMap = new HashMap<String,Integer>();
				Map<String,Boolean> boolMap = new HashMap<String,Boolean>();
				for(int i = 0; i < checkList.getItemCount(); i++){
					if(checkList.isSelected(i))
						boolMap.put(checkMembershipLabels.get(checkList.getItem(i).getText()), true);
					else
						boolMap.put(checkMembershipLabels.get(checkList.getItem(i).getText()), false);
				}
				if (!completedBox.getSelection())
					boolMap.put(Misc.STR_TIME + "#CurrentEvent", true);
				else{
					boolMap.put(Misc.STR_TIME + "#CompletedEvent", true);
					Calendar endDate = new GregorianCalendar(etime.getYear(), etime.getMonth(), etime.getDay());
					int endTime = Misc.calendarToInt(endDate);
					intMap.put(Misc.IRI_TIME + "#hasEnd", endTime);
				}
				Calendar beginDate = new GregorianCalendar(btime.getYear(), btime.getMonth(), btime.getDay());
				int beginTime = Misc.calendarToInt(beginDate);
				intMap.put(Misc.IRI_TIME + "#hasBeginning", beginTime);
				constraint = new Constraint(type, concrete, null, boolMap,
						intMap, true);
				try {
					if (type != "" && concrete != "")
						noerror = handler.addConstraint(constraint);
					else
						donothing = true;
				} catch (Exception e1) {
					error("Exception: " + e1.getMessage());
					e1.printStackTrace();
				}
				if (!noerror)
					error("Could not save changes because they would cause inconsistencies.");
				if (noerror && !donothing && c.getObject() != null)
					handler.removeConstraint(c);
				if (!donothing)
					newshell.dispose();
			}
		});
		comboObject.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				checkList.removeAll();
				checkList.setVisible(true);
				checkMembershipLabels.clear();
				final ArrayList<String> checkClasses = handler.getCheckMembershipClasses(maplabel.get(comboObject.getText()));
				if(checkClasses == null){
					checkList.setVisible(false);
					return;
				}
				for(String s : checkClasses){
					TableItem item = new TableItem(checkList, SWT.NONE);
					item.setText(handler.getLabel(s));
					checkMembershipLabels.put(handler.getLabel(s), s);
				}
				checkList.getColumn(0).pack();
				checkList.pack();
				newshell.pack();
			}
		});
		
		// Set defaults

		if (c.getObjectType() != null) {
			comboObject.setText(c.getObjectLabel());
		}
		checkList.removeAll();
		checkList.setVisible(true);
		checkMembershipLabels.clear();
		final ArrayList<String> checkClasses = handler.getCheckMembershipClasses(maplabel.get(comboObject.getText()));
		if(checkClasses == null){
			checkList.setVisible(false);
		}
		else{
			for(String s : checkClasses){
				TableItem item = new TableItem(checkList, SWT.NONE);
				item.setText(handler.getLabel(s));
				checkMembershipLabels.put(handler.getLabel(s), s);
				for (String key : c.getBoolMods().keySet()) {
					if (checkClasses.contains(key) && c.getBoolMods().get(key)){
						item.setChecked(true);
					}
				}
			}
			checkList.getColumn(0).pack();
			checkList.pack();
		}
		for (String key : c.getBoolMods().keySet()) {
			if (key.equals(Misc.STR_TIME + "#CompletedEvent")
					&& c.getBoolMods().get(key))
				completedBox.setSelection(true);
		}
		for (String key : c.getIntMods().keySet()) {
			if (key.equals(Misc.STR_TIME + "#hasEnd")){
				completedBox.setSelection(true);
				etime.setEnabled(true);
				etime.setDate(Misc.intToCalendar(c.getIntMods().get(key)).get(Calendar.YEAR), 
						Misc.intToCalendar(c.getIntMods().get(key)).get(Calendar.MONTH), 
						Misc.intToCalendar(c.getIntMods().get(key)).get(Calendar.DAY_OF_MONTH));
			}
			if (key.equals(Misc.STR_TIME + "#hasBeginning"))
				btime.setDate(Misc.intToCalendar(c.getIntMods().get(key)).get(Calendar.YEAR), 
						Misc.intToCalendar(c.getIntMods().get(key)).get(Calendar.MONTH), 
						Misc.intToCalendar(c.getIntMods().get(key)).get(Calendar.DAY_OF_MONTH));
		}
		

		newshell.pack();
		newshell.open();
		while (!newshell.isDisposed()) {
			if (!newshell.isDisposed()) {
				if (!display.readAndDispatch())
					display.sleep();
			}
		}
		return true;
	}
	
	/**
	 * Opens the precondition/additional information edit menu
	 * @param preconditions The list of preconditions to edit
	 */

	private static void preconditionMenu(String rec) {
		final Shell newshell = new Shell(display, SWT.SHEET);
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		newshell.setLayout(layout);
		newshell.setMinimumSize(500, 300);
		newshell.setText("Missing information");
		Monitor primary = display.getPrimaryMonitor();
		org.eclipse.swt.graphics.Rectangle bounds = primary.getBounds();
		org.eclipse.swt.graphics.Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		newshell.setLocation(x, y);

		final Table table = new Table(newshell, SWT.BORDER);
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true,
				true);
		gridData.horizontalSpan = 4;
		table.setLayoutData(gridData);
		table.setLinesVisible (true);
		table.setHeaderVisible (true);
		TableColumn column1 = new TableColumn (table, SWT.NONE);
		TableColumn column2 = new TableColumn (table, SWT.NONE);
		TableColumn column3 = new TableColumn (table, SWT.NONE);
		column1.setText("Question");
		column2.setText("Answer");
		column3.setText("Guideline section");

		final Button confirmButton = new Button(newshell, SWT.PUSH);
		confirmButton.setText("Yes");
		final Button excludeButton = new Button(newshell, SWT.PUSH);
		excludeButton.setText("No");
		final Button unknownButton = new Button(newshell, SWT.PUSH);
		unknownButton.setText("Unknown");
		final Button doneButton = new Button(newshell, SWT.PUSH);
		doneButton.setText("Done");
		
		Map<String, String[]> tableContent = new HashMap<String,String[]>();
		final ArrayList<Entry<String, String[]>> list = new ArrayList<Entry<String,String[]>>();
		ArrayList<String> recClasses = handler.getRecommendationClass(rec);
		for(String recClass : recClasses){
			if(checkPreconditions.get(recClass) == null)
				continue;
			ArrayList<String> annotations = handler.getGuidelineAnnotation(recClass);
			for(String[] pre : checkPreconditions.get(recClass)){
				String[] c;
				if(tableContent.containsKey(pre[0])){
					c = tableContent.get(pre[0]);
					for(String s: annotations)
						c[1] += ", " + s;
				}
				else{
					c = new String[2];
					c[1] = "";
					c[0] = pre[1];

					Iterator<String> iter = annotations.iterator();
					while(iter.hasNext()){
						c[1] += iter.next();
						if(iter.hasNext())
							c[1] += ", ";
					}
				}
				tableContent.put(pre[0], c);
			}
		}
		
			for (Entry<String, String[]> pre : tableContent.entrySet()) {
				list.add(pre);
				TableItem item = new TableItem(table, SWT.NONE);
				item.setText(0, handler.getLabel(pre.getKey()));
				String text = pre.getValue()[0];
				if(!handler.preconditionSatisfiedWithYes(pre.getKey())){
					if(text.equals("no"))
						text = "yes";
					else if(text.equals("yes"))
						text = "no";
				}
				item.setText(1, text);
				item.setText(2, pre.getValue()[1]);
			}
		
		
		table.getColumn(0).pack();
		table.getColumn(1).pack();
		table.getColumn(2).pack();
		table.getColumn(0).setWidth(500);
		table.getColumn(1).setWidth(100);
		table.getColumn(2).setWidth(150);
		doneButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				newshell.dispose();
			}
		});
		confirmButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = table.getSelectionIndex();
				if (i == -1) {
					System.out.println("No precondition selected");
					return;
				}
				switch (table.getItem(i).getText(1).charAt(0)) {
					case 'y':
						break;
					case 'n':
						handler.removeType(list.get(i).getKey(), !handler.preconditionSatisfiedWithYes(list.get(i).getKey()));
						table.getItem(i).setText(1, "unknown");
					case 'u':
						if (handler.isTrivial(list.get(i).getKey()) && handler.addType(list.get(i).getKey(), handler.preconditionSatisfiedWithYes(list.get(i).getKey()))) {
							table.getItem(i).setText(1, "yes");
						} else {
							error("This precondition is too complex, please confirm it manually.");
						}
				}
			}
		});
		excludeButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = table.getSelectionIndex();
				if (i == -1) {
					System.out.println("No precondition selected");
					return;
				}
				switch (table.getItem(i).getText(1).charAt(0)) {
					case 'n':
						break;
					case 'y':
						handler.removeType(list.get(i).getKey(), handler.preconditionSatisfiedWithYes(list.get(i).getKey()));
						table.getItem(i).setText(1, "unknown");
					case 'u':
						if (handler.isTrivial(list.get(i).getKey()) && handler.addType(list.get(i).getKey(), !handler.preconditionSatisfiedWithYes(list.get(i).getKey()))) {
							table.getItem(i).setText(1, "no");
						} else {
							error("This precondition is too complex, please exclude it manually.");
						}
				}
			}
		});
		unknownButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = table.getSelectionIndex();
				if (i == -1) {
					System.out.println("No precondition selected");
					return;
				}
				switch (table.getItem(i).getText(1).charAt(0)) {
					case 'u':
						break;
					case 'y':
						handler.removeType(list.get(i).getKey(), handler.preconditionSatisfiedWithYes(list.get(i).getKey()));
						break;
					case 'n':
						handler.removeType(list.get(i).getKey(), !handler.preconditionSatisfiedWithYes(list.get(i).getKey()));
				}
				table.getItem(i).setText(1, "unknown");
			}
		});

		newshell.pack();
		newshell.open();
		while (!newshell.isDisposed()) {
			if (!newshell.isDisposed()) {
				if (!display.readAndDispatch())
					display.sleep();
			}
		}
		return;
	}
}
