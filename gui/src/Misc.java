import java.io.File;
import java.util.Calendar;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLEntity;

public class Misc {

	private Misc() {}

	public static final String STR_PREFIX  = "http://www8.cs.fau.de/research:cgm/";
	public static final String STR_SCHIZO  = STR_PREFIX + "schizophrenia";
	public static final String STR_TIME    = STR_PREFIX + "time";
	public static final IRI    IRI_SCHIZO  = IRI.create(STR_SCHIZO);
	public static final IRI    IRI_TIME    = IRI.create(STR_TIME);

	private static final String BASE = Misc.class.getProtectionDomain().getCodeSource().getLocation().getPath(); 
	public static final String BASE_PATH   = BASE.substring(0,BASE.lastIndexOf("/"));
	public static final String PATH_SCHIZO = BASE_PATH + "/schizophrenia_rdf.owl"; 	
	public static final String PATH_TIME   = BASE_PATH + "/time_rdf.owl";
	public static final File   FILE_SCHIZO = new File(PATH_SCHIZO);
	public static final File   FILE_TIME   = new File(PATH_TIME);

	/**
	 * Gets the part after a # from an IRI string.
	 * @param iriString the IRI string
	 * @return the part after the last #; if there is no #, return the whole input
	 */
	public static String baseName(String iriString) {
		String[] parts = iriString.split("#");
		return parts[parts.length - 1];
	}

	/**
	 * Gets the base name of an OWLEntity.
	 * @param entity the entity
	 * @return the base name
	 * @see #baseName
	 */
	public static String entityName(OWLEntity entity) {
		return baseName(entity.toStringID());
	}

	/**
	 * Converts a date to the number of days since 1900/12/31.
	 * @param c calendar representing the date
	 * @return number of days
	 * @see #intToCalendar
	 */
	public static int calendarToInt(Calendar c) {
		int years = c.get(Calendar.YEAR) - 1901;
		// next not-leap-year will be 2100
		int leapyears = years / 4;
		return 365 * years + leapyears + c.get(Calendar.DAY_OF_YEAR);
	}

	/**
	 * Converts a number of days since 1900/12/31 to a date
	 * @param i the number of days
	 * @return calendar representing the date
	 * @see #calendarToInt
	 */
	public static Calendar intToCalendar(int i) {
		Calendar c = Calendar.getInstance();
		c.set(1900, Calendar.DECEMBER, 31);
		c.add(Calendar.DAY_OF_YEAR, i);
		return c;
	}

	/**
	 * Gets today's date.
	 * @return calendar representing the date
	 */
	public static Calendar today() {
		return Calendar.getInstance();
	}
	
}
