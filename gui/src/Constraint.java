import org.semanticweb.owlapi.model.*;

import java.util.Map;

/**
 * Describes a constraint of the form “PatientX hasY Z” with some information about Z
 */
public class Constraint implements Comparable<Constraint>{

	/**
	 * the most general superclass type Y of the object Z, either "iri#Symptom" or "iri#Diagnosis" or "iri#Measure"
	 */
	private String              objectSuperType;
	/**
	 * the concrete type of the object Z, for example "iri#Stupor"
	 */
	private String              objectType;
	/**
	 * the label of the object type in the schizophrenia ontology
	 */
	private String              objectLabel;
	/**
	 * a map from types to booleans; contains all technical classes like time:CompletedEvent or schizo:ObviousSymptom
	 */
	private Map<String,Boolean> boolMods;
	/**
	 * data properties of the object like duration
	 */
	private Map<String,Integer> intMods;
	/**
	 * the object as an individual
	 */
	private OWLIndividual       object;
	/**
	 * is this constraint in the current session?
	 */
	private boolean             isCurrentSession;

	public Constraint(String objectSuperType, String objectType, OWLIndividual object, Map<String,Boolean> boolMods, Map<String,Integer> intMods, boolean isCurrentSession) {
		this.objectSuperType  = objectSuperType;
		this.objectType       = objectType;
		this.object           = object;
		this.boolMods         = boolMods;
		this.intMods          = intMods;
		this.isCurrentSession = isCurrentSession;
	}

	public String getObjectSuperType() {
		return objectSuperType;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType){
		this.objectType = objectType;
	}

	public String getObjectLabel() {
		return objectLabel;
	}

	public void setObjectLabel(String objectLabel){
		this.objectLabel = objectLabel;
	}

	public OWLIndividual getObject() {
		return object;
	}

	public Map<String,Boolean> getBoolMods() {
		return boolMods;
	}

	public void setBoolMods(Map<String,Boolean> boolMods){
		this.boolMods = boolMods;
	}

	public Map<String,Integer> getIntMods() {
		return intMods;
	}

	public boolean isCurrentSession() {
		return isCurrentSession;
	}

	public int compareTo(Constraint o) {
		return this.intMods.get(Misc.IRI_TIME + "#hasBeginning") - (o.intMods.get(Misc.IRI_TIME + "#hasBeginning"));
	}

	public int getBegin() {
		if(intMods.get(Misc.IRI_TIME + "#hasBeginning") != null)
			return intMods.get(Misc.IRI_TIME + "#hasBeginning");
		else
			return -1;
	}

	public int getEnd() {
		if(intMods.get(Misc.IRI_TIME + "#hasEnd") != null)
			return intMods.get(Misc.IRI_TIME + "#hasEnd");
		else
			return -1;
	}

}
