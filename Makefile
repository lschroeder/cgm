.PHONY: all

MANS=$(wildcard *_man.owl)
RDFS=$(patsubst %_man.owl,%_rdf.owl,$(MANS))

all: SyntaxConverter.class $(RDFS) schizophrenia_rdf.owl time_rdf.owl

OWLAPI=gui/src/lib/owlapi-distribution-3.4.8.jar

SyntaxConverter.class: SyntaxConverter.java
	javac SyntaxConverter.java -cp ${OWLAPI}

schizophrenia_rdf.owl: schizophrenia.owl SyntaxConverter.class
	java -cp .:${OWLAPI} SyntaxConverter $< $@ rdf

time_rdf.owl: time.owl SyntaxConverter.class
	java -cp .:${OWLAPI} SyntaxConverter $< $@ rdf

%_rdf.owl: %_man.owl SyntaxConverter.class
	java -cp .:${OWLAPI} SyntaxConverter $< $@ rdf
