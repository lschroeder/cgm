# CGM -- The Clinical Guideline Module

CGM is a modular clinical decision support system using *OWL* ontologies as sole representation, with a Java front-end. Currently it supports parts of the *German S3 guideline for schizophrenia* (ICD10/F20).


