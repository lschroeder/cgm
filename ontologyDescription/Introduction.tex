\section{Introduction}
	
In this report we describe our results of the ``Praktikum Knowledge Representation'' 
which took place in the winter term 2013/2014. 
Our task was to create a prototype of a knowledge-based support system for the application in psychiatries.
The system is based on clinical practice guidelines (CPGs). We used the schizophrenia S3 guideline (SG) of the
German Association for Psychiatry, Psychotherapy and Psychosomatics (DGPPN)~\cite{dgppn:125}.
Our tool is called \tool where CGM means Clinical Guideline Module and F20 refers to the ICD10 diagnostic code for schizophrenia.
We formalized the guideline using the Web Ontology Language (OWL). The front-end of our system is written in Java.

At first we present a use case of our tool to familiarize the reader with its functionalities. 
Afterwards we give a short overview on the structure of the underlying system, namely the used ontologies and the 
architecture of the implementation.
We then proceed to describe in detail the ontologies (section~\ref{sec:Ontologies}), the API usage and used queries (section~\ref{sec:Reasoning})
and the implementation of the front-end (section~\ref{sec:Implementation}). 

\subsection{Use case}
Before describing the details of our project, we present a possible use case of the \tool 
to briefly introduce the functionalities of the \tool and the schizophrenia guideline.
In all cases, the user of the system is assumed to be a physician working at the 
medical institution where the patient is admitted.

The main screen of the user-interface (UI) shows the medical record of a patient
(see figure~\ref{fig:history});
this can be thought of as a time-ordered sequence of events such as symptoms reported,
diagnoses performed, results of tests, prescription of medication and other therapeutic measures etc.
Events have a starting date, but need not have an
end, in which case they are considered as ongoing.
The medical history is split into two parts: recent and past history.
Everything mentioned in the recent history is part of the ongoing process of treating the patient,
while events shown in the past history are seen as part of a completed therapy.


\begin{figure}
  \includegraphics[width=\textwidth,trim=0cm 11cm 0cm  0cm, clip]{s1.png}
  \caption{Display of the medical history of a patient.}
  \label{fig:history}
\end{figure}

The physician can, of course, consult the patient's history or record new facts in it, as in a regular hospital information system (HIS).
The novel feature would be in the \emph{Guideline Assistant} tab
where the system suggests diagnoses and measures in accordance with the guideline. To see how this works, let us
assume that a patient has been recently admitted and it is clear that he
has been hearing hallucinatory voices commenting on his behaviour for a prolonged time.
Because of additional symptomatology and patient background, he is considered as possibly HIV positive.
The DGPPN-SG stipulates:

\begin{quotation}
  In order to diagnose schizophrenia one requires at least one
  \emph{unmistakable} symptom from Groups 1 to 4 (or two, if they are
  less certain), or at least two symptoms from Groups 5 to 8.  These
  symptoms have to be constantly present for a month or
  longer.~\cite[pp.\ 31--32]{dgppn:125}
\end{quotation}
(translated from the original German, as are all further guideline
quotes; our emphasis).  Let us further assume that it has been
recorded in the patient's record that he ``unmistakably'' hears
commenting voices (as shall be discussed in the next section, it is
not immediately obvious how to handle such symptom-modifiers in a
sensible and generic way).  Since according to the DGPPN-SG, this is a
symptom in Group 3, the system should recommend the diagnosis of
schizophrenia. Figure~\ref{fig:missing-precond} shows the
recommendation of the system: it mentions ``acute schizophrenia'' as a
possible diagnosis, but indicates that the recommendation is cannot
yet be made definite due to missing information. This follows from the guideline,
which not only lists exceptional cases under which the diagnosis
should not be performed:
\begin{quotation}
        One should not diagnose schizophrenia in the presence of an unmistakable brain disorder
        or while the patient is intoxicated or undergoing detoxification.~\cite[p.\ 32]{dgppn:125}
\end{quotation}

but also dictates a differential diagnosis to rule out alternative psychotic disorders.
In addition to the putative diagnosis, the system recommends a \emph{measure}, namely to perform a test for HIV; this again follows
from the guideline:
\begin{quotation}
        %(2) Bei entsprechendem Verdacht sollte ein HIV-Test [\ldots] erfolgen.
        In case of a corresponding suspicion, an HIV test [\ldots] should be performed.
\end{quotation}
Note in passing that the interface provides the user with a reference for the section of the
abridged version of the guideline on which the recommendation is based.


At this point the physician could, for instance, immediately dismiss ``detoxification'' as a
possibility (this information would then become part of the patient's electronic health record), and then order blood tests
and a brain scan to rule out the remaining obstacles to the diagnosis. If the
patient's medical record were then updated with negative results for all tests, the other
scenarios would vanish and the system would then actually recommend  the  diagnosis
of acute schizophrenia.

\begin{figure}
  \includegraphics[width=\textwidth]{s2.png}
  \caption{A conformant diagnosis requires exceptional conditions and alternative scenarios to be ruled out.}
  \label{fig:missing-precond}
\end{figure}


We stress that this would be just a recommendation, that is, the diagnosis would not be automatically
issued by the system. So let us further assume the physician agrees with this diagnosis and
records it in the system. The DGPPN-SG recommends a pharmacotherapy, but only as long as the
patient agrees
 to it (while in a sound state of mind):
\begin{quotation}
        At the beginning of a pharmacotherapy, the patient must be informed about
    both the therapeutic and adverse effects of the medication, and
        the patient should be included in the process of making therapeutic
    decisions~\cite[p.\ 43]{dgppn:125}
\end{quotation}
This is in fact the recommendation we now receive from the system (figure~\ref{fig:rec-measure}).
There are no exceptions or alternative scenarios to be considered in this case, so no additional
information is needed to actually recommend this measure. The system will not recommend to
start anti-psychotic therapy until the informed consent from the patient is recorded.


\begin{figure}
    \includegraphics[width=\textwidth,trim=0cm 2.5cm 0cm 0cm,clip]{s3.png}
    \caption{Display of the recommendation of measures.}
    \label{fig:rec-measure}
\end{figure}

\subsection{Structure} \label{ssec:structure}
The program consists of two parts, the front-end (section~\ref{sec:Implementation}) and the underlying ontologies (section~\ref{sec:Ontologies}). 
We briefly describe the structure of both of them.

As our tool is based on the schizophrenia guideline we of course need a formal description of the 
knowledge represented in it. Therefore we have the schizophrenia ontology (section~\ref{ssec:schizoontology}) which can been seen 
as set of classes and properties or in terms of ontologies as the TBox.
We also have to store the medical record of a patient, therefore we create a seperate ontology 
for each patient which contains the data we know about them (section~\ref{sec:Patient Ontology}). 
In contrast to the schizophrenia ontology it is a set of individuals and facts called ABox.
As seen in the use case schizophrenia should only be diagnosed if the symptoms are present for at least a month.
Hence we need a representation of time. Since it has nothing to do with the guideline itself, we wrote 
another ontology describing time (section~\ref{ssec:time ontology}).


In order to make recommendations we have to run queries on the ontologies, for example
to get recommended diagnoses and recommended measures.
We use SPARQL\footnote{http://www.w3.org/TR/rdf-sparql-query/} as a query language and 
Pellet\footnote{http://clarkparsia.com/pellet/} as a reasoner (section~\ref{sec:Queries}).

The front-end of the \gentool is split in three main parts. 
One of the parts is the user interface one can see on the screenshots (section~\ref{ssec:Owlgui}). 
It is implemented in the class Owlgui.java and uses SWT\footnote{http://www.eclipse.org/swt/}.
Another part is the class OWLHandler.java which runs queries and analyzes the schizophrenia ontology (section~\ref{ssec:OWLHandler}). 
We use the Jena API\footnote{https://jena.apache.org/} to be able to run SPARQL queries. 
Last but not least the class Patient.java performs CRUD operations on the ontology of the patient (section~\ref{ssec:Patient}).
It is for example used to add symptoms to their ontology. 
Both, the Patient and the OWLHandler class, use the OWL API\footnote{http://owlapi.sourceforge.net/} to access the ontologies.

