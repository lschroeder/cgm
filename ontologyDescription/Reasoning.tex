After having described the ontologies we now proceed to explain the queries that are used to get recommendations and
to check the status of preconditions. After that we give a short overview on the used APIs before
going into detail about the implementation in the next section.

\subsection{Queries}
\label{sec:Queries}

The reasoning service by means of which \tool achieves
recommendations is querying via SPARQL-DL formulas~\cite{SirinParsia07}.
Ideally, one would want to, say, query for classes \lstinline{C} of diagnoses
such that the patient at hand belongs to the class
\lstinline{hasRecommendedDiagnosis some C}; however, constructs of this type  
go beyond the expressive power of SPARQL-DL (actually, \textsc{Pellet}~\cite{SiPaCuKaKa05:pellet-SHORT}
sometimes succeeds in solving such queries when expressed in SPARQL, but 
the precise semantics of this is not well-documented and we
found the overall behaviour too fragile: the output varies, for instance
with the order of the \lstinline{WHERE}-clauses).
Instead, we populate the relevant classes of
measures and diagnoses with generic individuals, which we use in place
of the class in the definition of recommendation classes; for instance, we use 
an individual \lstinline{R_Schizophrenia} instead of the class
\lstinline{Schizophrenia}. We then query for individuals that are,
say, diagnoses, and are related to the patient at hand via 
\lstinline{hasRecommendedDiagnosis}. For example, if the system was asked to
produce diagnostic recommendations for a patient, the query produced would be the one in figure~\ref{fig:QueryRecommendedDiagnoses}.
%To be able to make recommendations we have to check whether an individual of the class Patient is in relation to 
%a static recommendation individual. Recommendations are implemented in terms of object properties 
%\emph{hasRecommendedDiagnosis,} etc. As described in Section~\ref{ssec:Recommendations} recommendation classes are always subclass of a 
%class expression of the form \emph{hasRecommendedDiagnosis value R_SomeDiagnosis} where \emph{R_SomeDiagnosis} could
%for instance be \emph{R_AcuteSchizophrenia}. Note that \emph{R} is an abbreviation for recommendation.
%Recommendation classes for measures have the same format but of course use \emph{hasRecommendedMeasure} instead.
%We also have non-recommendation classes to overwrite recommendation, they also use the format just with
%\emph{hasNonRecommended} instead of \emph{hasRecommended}.
\begin{figure}[h!]
	\begin{lstlisting}
SELECT ?diagnosis WHERE {
	Argan schizophrenia:hasRecommendedDiagnosis ?diagnosis.
	FILTER NOT EXISTS {Argan schizophrenia:hasNonRecommendedDiagnosis ?diagnosis}.}
	\end{lstlisting}
	\caption{Query for recommended diagnoses}
	\label{fig:QueryRecommendedDiagnoses}
\end{figure}

These queries are always adapted for a certain patient, in this example for a patient called Argan.
That query first gets all recommended diagnoses and then removes the non-recommended ones. 

For example a patient with an unmistakable symptom from a certain
list of symptoms will satisfy  \emph{hasRecommendedDiagnosis R_AcuteSchizophrenia}, 
but the diagnosis will not be recommended  if that diagnosis has already been made, 
putting the patient also in the class \emph{hasNonRecommendedDiagnosis R_AcuteSchizophrenia},
so that \emph{R_AcuteSchizophrenia} is removed from the result of the query.

The guideline states that one should not diagnose schizophrenia if a patient has a brain disorder. 
As we already mentioned recommendation classes are implications, so our first approach was to include 
the statement \emph{not(hasDiagnosis some BrainDisorder)} in left side of the implication. 
In this case schizophrenia in only recommended for patients for which we already know they have no brain disorder. 
This has the one important disadvantage that we need to know in advance that a patient has no brain
disorder to be able to make a recommendation. From a technical point of view this is because our
system is based on the \emph{Open World Assumption}, which means that as long as we do not enter
any information about a patients brain disorder he is neither in the class 
\emph{hasDiagnosis some BrainDisorder} nor in the class \emph{not(hasDiagnosis some BrainDisorder)}.
We solved this problem by introducing precondition classes already mentioned in section~\ref{ssec:Recommendations}.

So having established diagnoses to be explored by the above query, we next need to
check the status of the preconditions of the relevant recommendations before a diagnosis
can be conclusively recommended. 

Therefore we query for all classes a patient belongs to, using the query
%\begin{figure}[h]
	\begin{lstlisting}
SELECT ?class WHERE {Argan IS_A ?class.}
	\end{lstlisting}
%	\label{fig:QueryForAllClasses}
%	\caption{Query for all classes a patient belongs to}
%\end{figure}
As the answer to this query contains all classes the patient belongs to, it also contains all precondition 
classes the patient satisfies. We then iterate over all preconditions of recommendation
classes the patient belongs to and check their status:
\begin{itemize}
\item If the precondition occurs in the above query, its status is ``confirmed''; 
for instance because one entered the fact that the patient has no brain disorder.
\item If the patient is in the negation of the precondition class the
  status is ``excluded''. For a precondition represented by
  \lstinline{SomePreconditionClass}, we can check this using% the query
% PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
% PREFIX owl: <http://www.w3.org/2002/07/owl#>
% PREFIX schizophrenia: <http://www8.cs.fau.de/ws13:krlab/schizophrenia#>
%  <http://www8.cs.fau.de/ws13:krlab/PatientName#PatientName>
%\begin{figure}[h]
\begin{lstlisting}
ASK {Argan IS_A [owl:complementOf schizophrenia:SomePreconditionClass].}
\end{lstlisting}
%\caption{Query for complement of a precondition class}
%\label{fig:QueryForComplement}
%\end{figure}
This could be the case for a patient having a brain disorder.

\item Otherwise the status of the precondition is ``unknown''. This is the case if
the patient is neither in the precondition nor in its negation. 
As seen in the use case a doctor can now manually include or exclude the 
patient of this precondition class.
\end{itemize}



\subsection{APIs}		
After describing the queries we now briefly explain the APIs before going into details about the implementation.

As already mentioned in section~\ref{ssec:structure} we use two different APIs to handle the ontologies, the OWL and the Jena API.
The main difference is that the OWL API does not support queries, so we use the Jena API to run the queries seen above.

The queries are hard coded as strings where we only have to enter the name of the patient and, for the last query shown above, the  
name of the precondition class. The queries are run on the patient ontology which imports the schizophrenia and the time ontology.
Pellet is used as a reasoner.

We use the OWL API for creating and updating the ontology of the patient and for handling the static information in the schizophrenia 
ontology. For example we mentioned above that we iterate over all preconditions of recommendation classes the patient belongs to. 
This involves two steps, at first we have to resolve the answer of the query for recommended diagnoses, for instance 
\emph{R_AcuteSchizophrenia}, to the name of the related recommendation class, for instance \emph{RecommendedDiagnosisAcuteSchizophrenia}.
Afterwards we simply get all preconditions because they are annotated to the recommendation class.
We also use the OWL API for purposes like getting the value of the rdfs:label annotation and the guideline annotation or retrieve
a list of all symptoms that are subclasses of the class \emph{Concrete}.

