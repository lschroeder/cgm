\documentclass{article}
\usepackage{amsthm, graphicx, times,xspace, relsize, lipsum, underscore, listings, tikz}
\usetikzlibrary{calc,trees,positioning,arrows,chains,shapes.geometric,%
    decorations.pathreplacing,decorations.pathmorphing,shapes,%
    matrix,shapes.symbols}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\newcommand{\tool}{{\smaller CGM/F20}\xspace}
\newcommand{\gentool}{{\smaller CGM}\xspace}
\usepackage{ellipsis}
%\usepackage{underscore}
\usepackage{geometry}
\usepackage{floatpag}

\usepackage{listings}
\lstdefinelanguage{MY_SPARQOWL} % something we use both for OWL and SPARL
  {
   keywords={Individual, Types, Facts, Class, Annotations,
             EquivalentTo, SubClassOf, Rule,
             and, some, value, not, true, false,
             SELECT,WHERE,FILTER,NOT,EXISTS,PREFIX,ASK}
  ,otherkeywords={}
  ,literate={schizophrenia:}{\smaller{\color{gray} schizophrenia{$:$}}}1
            {time:}{\smaller{\color{gray} time{$:$}}}1
            {cgm:}{\smaller{\color{gray} cgm{$:$}}}1
            {rdf:}{\smaller{\color{gray} rdf{$:$}}}1
            {rdfs:}{\smaller{\color{gray} rdfs{$:$}}}1
            {owl:}{\smaller{\color{gray} owl{$:$}}}1
            {IS_A}{{\bf a}}1
  ,sensitive=true
  ,tabsize=2
%  ,morecomment=[l]{//}
%  ,morecomment=[s]{/*}{*/}
  ,morestring=[b]"
  }
\lstset{%
  language=MY_SPARQOWL
   ,xleftmargin=0.1cm
   ,basicstyle=\small
   ,commentstyle=\color{lightgray}
   ,stringstyle=\itshape   % string literal style
   ,keepspaces=true                 % keeps spaces in text, useful for keeping indentation of code
   ,numbers=none                    % possible values are (none, left, right)
  %,identifierstyle={\itshape}
   ,flexiblecolumns={true}
   ,mathescape={true}
   ,captionpos=b
   ,emph={hasSymptom,hasSession,hasBeginning,hasEnd,hasDuration,subtract,hasDiagnosis,hasTherapyGoal,hasRecommendedDiagnosis,hasNonRecommendedDiagnosis,hasSuspectedDiagnosis,fulfillsTherapyGoal,guideline,checkMembership,comment,label,precondition,trivial,satisfiedWithYes}
   ,emphstyle=\slshape
}
\usepackage{xcolor, csquotes, graphicx}

\title{Project Report}
\author{Ulrich Rabenstein, Malte Meyn, Miriam Polzer}

\begin{document}
\maketitle

\input{Introduction.tex}
\the\textwidth
\section{Ontologies}
	\label{sec:Ontologies}
	\input{Ontologies.tex}
	
\section{Reasoning and APIs}
	\label{sec:Reasoning}
	\input{Reasoning.tex}

\section{Implementation}
	\label{sec:Implementation}
In this section we describe details of our implementation, for a full class diagram see figure~\ref{fig:uml1} and figure~\ref{fig:uml2}.
We already explained queries and mentioned the APIs, so we start to introduce the class \emph{OWLHandler.java}
as it implements queries and uses both APIs. The \emph{OWLHandler} mentions \emph{Constraint}s, so we proceed to
explain them before describing the class \emph{Patient}. Afterwards we have a short look at the class \emph{Misc}. 
Last but not least the user interface is explained.

\begin{figure}
	\includegraphics[width=\textwidth]{uml1.png}
	\caption{UML class diagram of the UI}
	\label{fig:uml1}
\end{figure}
\begin{figure}
	\includegraphics[width=\textwidth]{uml2.png}
	\caption{UML class diagram of the UI}
	\label{fig:uml2}
\end{figure}
\subsection{The class OWLHandler}
	\label{ssec:OWLHandler}
The task of the OWLHandler is to handle the static ontologies time and schizophrenia, to perform queries and
to be an interface between the classes \emph{Owlgui} and \emph{Patient}.

At the start of the program OWLHandler runs once over all classes in the schizophrenia ontology and saves static relations, like 
for example the connection between classes and their english rdfs:label annotation value and 
between recommendation classes and their preconditions. 
These information is stored in maps where the key is \emph{IRI\#ClassName}.
IRI is an abbreviation for internationalized resource identifier, for instance the IRI of the schizophrenia ontology is 
\emph{http://www8.cs.fau.de/research:cgm/schizophrenia}. 
The value is either the value of the annotation or an ArrayList of annotation values. 
For instance we assume every class to have only one rdfs:label annotation with english language tag whereas
a recommendation class might have multiple preconditions.
For details have a look at the \emph{OWLHandler.init} method.
There are getters for each of these maps such that Owlgui can access the content, for example \emph{OWLHandler.getLabel}. 

The methods \emph{OWLHandler.getSymptoms/Diagnoses/Measures} return a list of all classes being subclass of \emph{Symptom/Diagnosis/Measure} 
and having the annotation \emph{Concrete}. Recall that \emph{ThoughtInsertion} is concrete while \emph{Symptom1} isn't.

Another task of the OWLHandler is querying. Like already mentioned we use the Jena API for it.\\
For example \emph{OWLHandler.queryForRecommendedDiagnoses} returns the result of the query in figure~\ref{fig:QueryRecommendedDiagnoses}.
Queries are encoded as \emph{ParameterizedSparqlString}s. The name of the patient individual is entered in the format
\emph{<IRI>\#Name}. Note that the IRI refers to the ontology of the patient and not to the schizophrenia ontology. 
Name is simply the name of the patient. 
Querying returns a \emph{ResultSet}. In case of \emph{OWLHandler.queryForRecommendedDiagnoses} the query returns a 
set of recommendation individuals like \emph{R_AcuteSchizophrenia}, so we process the output and return a list of
classes of diagnoses, for instance \emph{AcuteSchizophrenia}. 

The OWLHandler has a patient attribute whose object is loaded or created using the methods \emph{OWLHandler.addPatient} or \emph{OWLHandler.loadPatient}. 
It's also the job of the OWLHandler to create the path of the ontology of the patient, we currently use the name of the patient as filename. 
Methods like \emph{OWLHandler.addType} and \emph{OWLHandler.addConstraint}, which affect the ontology of the patient,
just pass their arguments through to the suitable methods of the class \emph{Patient}. 

\subsection{Constraint}
As we already mentioned \emph{Constraint}s we explain this class in detail now.
\emph{Constraint} is a container class for facts about the patient.
For instance after recording the symptom commenting voices a constraint object is created to pass this information 
to the class \emph{Patient} which adds a suitable individual to the ontology of the patient.
Hence a constraint must store the type of the object for instance \emph{IRI\#CommentingVoices}.
It also consists of a boolean modifier map, where the key is a string and the value is a boolean.
For example the key ``unmistakable'' and the value true state that the symptom is unmistakable. 
The start and possibly the end date are stored in a similar map whith values of type integer. 
Note that constraints are also used to pass information in the other direction, namely from the 
class \emph{Patient} to \emph{Owlgui}. For that a constraint also contains the super type, 
which is either \emph{IRI\#Diagnosis}, \emph{IRI\#Symptom} or \emph{IRI\#Measure}, and the
value of the rdfs:label annotation.
Last but not least a constraint may also contain the \emph{OWLIndividual} which it represents.
This is for instance used to add an end date to an already existing individual.

\subsection{The class Patient}
	\label{ssec:Patient}
The class \emph{Patient} makes changes directly in the ontology of the patient. 
The constructor can create a new patient ontology or load an existing one from file. 
Individuals in the ontology need to have unique names for that the constructor gets the
number of individuals in the ontology. 
If the ontology is loaded from file, the patient individual is identified (assumption: individual and ontology have the same name) 
and from all the session individuals we find the (only) current one. 
Durations of current events are updated based on start date and current date. 
There are also a public save method and a private reload method (the latter is used to update the ontology before checking consistency). 
The method \emph{Patient.keepAllIndividualsDifferent} adds an axiom that declares all individuals as different; 
this is needed for correct reasoning (for example, sessions have to be different). 
The methods \emph{Patient.add/exclude/removeConstraint} and \emph{Patient.getAllConstraints} do what the name suggests; 
\emph{Patient.excludeConstraint} adds the fact that a constraint is explicitely not satisfied. 
The method \emph{Patient.endConstraint} sets the end date of a selected constraint. 
When a patient is released, \emph{Patient.endSession} is called; this makes the current session a past one and adds a new current session. 
\emph{Patient.addType}, \emph{Patient.removeType} and \emph{Patient.getTypes} refer to the types of the patient individual; i.\,e.\ Patient and possibly precondition classes or their complements.
For example, a patient individual could look like this:
\begin{lstlisting}
Individual: Argan
	Types:
		schizophrenia:Patient,
		(not schizophrenia:PreconditionNoIntoxication),
		schizophrenia:PreconditionNoBrainDisorder
\end{lstlisting}
Then Patient.getTypes returns all of the three types as pairs of type names and booleans (for the possible negation), but not other classes like \lstinline{owl:Thing}.
\subsection{Misc}
\emph{Misc} is a library to store static information like the IRIs and the file paths of the time 
and the schizophrenia ontology.
It also offers functions to convert a date (as Java's Calendar) to days since 1900/12/31 and the other way around.

\subsection{Owlgui}
	\label{ssec:Owlgui}

%\begin{description}
%		\item[Owlgui: ]
%				This is the main class, it provides the frontend as SWT-Interface and handles all interaction with the user.
%		\item[OWLHandler: ]
%				The OWLHandler accesses the guideline and time ontologies and opens and closes patients. It handles all queries (either with pellet or simple ones with the API) and forwards required changes in the patient to its Patient object. Details are below.
%		\item[Patient: ]
%				This class accesses and changes the ontology of the current patient and creates new patient ontologies. For a more detailed description, see below.
%		\item[Constraint: ]
%				Container class for "Constraints", that represent a fact about the patient. These facts have at least a Label (display name for the UI), Type, a superType (one of Diagnosis, Measure, Symptom or suspectedDiagnosis) and a start date. It is possible to add generic information with the int and boolean modifiers lists, those lists store Strings with corresponding integer or boolean value. For example when adding an "unmistakable" syptom, the boolean list holds "unmistakable" and true.
%		\item[Misc: ]
%				A library to store all static information and functions, for example IRI-Names and a function to get the current date as days since 1/1/1990.
%		\item[Pair: ]
%				Storage of pairs of data with different datatypes, as there is no java class for it.
%		\item[PatientNotFoundException and PatientAlreadyExistsException: ]
%				Exceptions to specify why a patient could not be loaded or created.
%\end{description}

New patient ontologies can be created, open ones saved, and old ones loaded with the navigation bar. It uses the \emph{Owlgui.loadMenu} for a file dialogue.
The Interface has three tabs, namely \emph{Recent history}, that shows all constraints of the current
session and all preconditions, \emph{Past history}, for all constraints of past sessions and
\emph{Guideline assistant}, making recommendations.
All shown text from the ontologies is received by \emph{OWLHandler.getLabel}.

\subsubsection{Recent History Tab}
	This view has two tables, one for the recent history and one for given additional information (preconditions).\par
	The recent history table shows all constraints of the patient in the current session, it is filled by \emph{Owlgui.fillHistoryTable}. All constraints of the current patient are received by \emph{OWLHandler.getAllConstraints}, the ones that occured in the current session
	(\emph{Constraint.isCurrentSession}) are added to the table.\par
	To add new constraints, there is a button for every super type. If one of those or the edit button is pushed,
	a new shell is opend by \emph{Owlgui.constraintMenu}. This method gets a constraint as parameter that determines the default setting. It contains only the super type for new constraints,
	or the selected constraint that should be edited, as editing is done by deleting a constraint and creating a new one. We did this because as far as we know there are no axiom editing methods in the OWL API.
	For example when editing a symptom \emph{Thought Withdrawal}, the method \emph{Owlgui.constraintMenu} gets a Constraint object with the \emph{objectSuperType} Symptom and the \emph{objectType} \emph{<IRI>\#ThoughtWithdrawal}, where IRI is the 
	internationalized resource identifier of the ontology used to model the guideline.
	The object contains the start date and, if set, the end date as pairs in the integer modifier list as well.\par
	The menu allows to choose a type, start date, and end date. 
	The possible types are determined by \emph{OWLHandler.getDiagnoses/Symptoms/Measures}.
	Furthermore, it has a generic checklist for boolean modifiers, that currently only contains the \emph{checkMembership classes}
	obtained by \emph{OWLHandler.getCheckMembershipClasses}.\par
	The second table shows all answers, that where already given to the precondition questions. It is filled by \emph{Owlgui.fillPreconditionsTable} using \emph{OWLHandler.getExplicitPreconditions}.
	They can be revoked with the \emph{Remove selection} button.
	\subsubsection{Past History Tab}
		This tab has only one table that shows all constraints that occured in a past session.
		Table entries are created similar to the recent history with \emph{Owlgui.fillHistoryTable}, the last parameter of this function has to be false this time.
		Currently, it is only possible to set an end date to events from past sessions if they
		did not already have one. To do that, the method \emph{Owlgui.dateMenu} opens
		a new window that allows to choose a date. Once the date is chosen, \emph{OWLHandler.endConstraint} applies it to the constraint.
	\subsubsection{Guideline Assistant Tab}
		Again, we have a table. It shows all recommendations given by \emph{Owlgui.fillRecommendationsTable}.
		This method creates a list with all recommendations using \emph{OWLHandler.queryForRecommendedDiagnoses} and \emph{OWLHandler.queryForRecommendedMeasures}, that use pellet queries.
		A list of all recommendations' preconditions and their state (``yes'', ``no'' or ``unknown'') is created with the method \\ \emph{OWLHandler.queryForPatientInPreconditionClass}.
		For all recommendations \emph{Owlgui.fillRecTableHelper} checks the preconditions state. Since an answer ``yes'' to a question can mean either ``the precondition class is something negative, so the patient doesn’t satisfy the precondition'' or ``the precondition class is something positive, so the patient satisfies the precondition'', the \emph{satisfiedWithYes} annotation needs to be consulted. Now for every recommendation the following is checked:
		\begin{itemize}
			\item If one of the questions is answered with ``no'' and satisfiedWithYes is true or one is answered ``yes'' and satiesfiedWithYes is false, the recommendation is not included in the table, as it is impossible.
			\item If every question with satisfiedWithYes set to true is answered ``yes'' and every question with satisfiedWithYes set to false is answered ``no'', the recommendation is marked as confirmed. 
			\item Else it is marked as ``Possible (missing information)''.
		\end{itemize}
		The ``Guideline'' column either shows all possible sections for a recommendation if information is missing or all sections that confirm this recommendation, as enough information is given. That means, given an example with two sections that possibly give a recommendation, if both require additional information, both guideline sections are shown, but
		if only one of those is already confirmed, as all preconditions are of state ``no'', only its guideline section is displayed.\par
		For all recommendations, it is possible to open a new window showing the required preconditions and their state. This window is handled by the \emph{Owlgui.preconditionsMenu} method.
		As there might be more than one section for a recommendation, for all preconditions a list of the guideline sections, that require them is created.

		The method \emph{OWLHandler.queryForPatientInPreconditionClass} lists all preconditions and their state, which can be shown in a new window by using the method \emph{Owlgui.preconditionsMenu}. In this window the preconditions are shown as questions which can be answered by the user. To avoid double negation etc.\ in these questions, the answer ``yes'' to a question can either mean that a precondition is satisfied or that it isn't; this is determined by looking at the annotation \emph{satisfiedWithYes} in the ontology.
	\section{Future Work}
	Here are some suggestions for further improvements. For information on their progress and to work on them get access to the Bitbucket repository\footnote{https://bitbucket.org/lschroeder/cgm/}. 
	The order does not display the priority.
	\begin{itemize}
			\item Edit, add and remove constraints from all sessions. Look at tickets \#40 and \#41 for details. 
				The possibility to change the session of a constraint might also be interesting.
			\item It might also be useful to model that a patient did not have a symptom, diagnosis or a measure for a certain time. 
			For example a doctor could ask ``Did you have fever during the last four weeks?''. Modelling the negative answer is ticket \#46.
			
			\item As shown above patients can have certain therapy goals according to their diagnoses which should be displayed in the interface.
			The therapy goals should be connected to measures using the object property \emph{fulfillsTherapyGoal}. 
		 	So the interface should have a function to add this fact to the ontology of the patient. The list of therapy goals should
			distinguish between fulfilled goals and goals which are not fulfilled yet. 
			There can be a reason for not applying a measure to a certain therapy goal. 
			For example we don't have to inform a patient about his disorder if he was already informed before. 
			Therefore we introduced the class \emph{Reason}. Individuals of that class can also fulfill therapy goals.
			The interface should also support this feature.

			\item Constraints have a map of boolean and integer modifiers. 
			For example the boolean modifiers map for a symptom can contain the key ``unmistakable'' and the boolean ``true'', 
			stating that the symptom is unmistakable. The constraint menu displays this in a checklist. 
			For a future version this could be generalized to the integer modifiers, 
			as they are currently only used for start and end date. 
			For example a diagnosis might have a severity scale. 
			The constraint menu should then work for the integer modifiers in the same way as it already does for the boolean modifiers, 
			which means asking for information in the constraint menu and passing it to the Patient class which adds it to the ontology.
			\item Add an ``explanation'' button that displays the reason for a recommendation,
			possibly by parsing the output of pellets explain function. 
			This is ticket \#50, but there are no details because we only discussed it shortly and had no time to try out something.
			\item Ticket \#53 states that we should improve the coverage of the schizophrenia guideline. 
			Therefore we opened a group of other tickets, each concerning a different part of the guideline.
				\begin{itemize}
					\item \#56 concerns the chapter 2, advice (2) in the guideline. 
					For modelling this part we have to check whether the patient has schizophrenia for the first time. 
					Afterwards we can recommend the listed tests.
					\item \#57 concerns advice (21), but still needs discussion.
					\item \#55 concerns advice (74). We chose it because we thought it might be easy to model. 
					Nevertheless if there are too many difficulties it might be useful to skip this part and focus on another part first.
				\end{itemize}
			\item The guideline contains a rank for evidence which we currently ignore. 
				Modelling it in the ontology and afterwards displaying it in the interface is the aim of ticket \#9.
			\item We want to check whether our structure is also useful for other medical guidelines. 
			Therefore one could add another guideline to our system (\#26). 
			Note that this changes the whole ontology structure because 
			one needs to extract the general classes and object properties to a ``master ontology'' and adapt the implementation.
	\end{itemize}

\bibliographystyle{myabbrv}
\bibliography{paper}


\end{document}
