import java.io.File;
import java.io.FileOutputStream;

import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.vocab.PrefixOWLOntologyFormat;
import org.coode.owlapi.turtle.TurtleOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxOntologyFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.util.SimpleIRIMapper;

public class SyntaxConverter {
	public static void main (String[] args) throws Throwable {
		// WARNING: this is extremely ugly. And the main method throws Throwable because I didn’t want to search the API documentation for all the possible Exceptions …");
		if (args.length < 3) {
			System.out.println("Usage: java Converter in.owl out.owl (turtle|xml|manchester)");
			return;
		}

		OWLOntologyFormat outformat;
		if ("turtle".startsWith(args[2])) {
			outformat = new TurtleOntologyFormat();
		} else if ("xml".startsWith(args[2]) || "rdfxml".startsWith(args[2])) {
			outformat = new RDFXMLOntologyFormat();
		} else if ("owlxml".startsWith(args[2])) {
			outformat = new OWLXMLOntologyFormat();
		} else if ("manchester".startsWith(args[2])) {
			outformat = new ManchesterOWLSyntaxOntologyFormat();
		} else {
			System.out.println("Don’t know format " + args[2]);
			return;
		}

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		manager.addIRIMapper(new SimpleIRIMapper(IRI.create("http://www8.cs.fau.de/research:cgm/time"), IRI.create(new File("time.owl"))));
		manager.addIRIMapper(new SimpleIRIMapper(IRI.create("http://www8.cs.fau.de/research:cgm/schizophrenia"), IRI.create(new File("schizophrenia.owl"))));

		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(args[0]));
		OWLOntologyFormat informat = manager.getOntologyFormat(ontology);
		System.out.println("Loaded ontology in " + informat + " format");

		if (informat.isPrefixOWLOntologyFormat()) {
			((PrefixOWLOntologyFormat) outformat).copyPrefixesFrom(informat.asPrefixOWLOntologyFormat());
		}
		manager.saveOntology(ontology, outformat, new FileOutputStream(args[1]));
		System.out.println("Saved ontology in " + outformat + " format");
	}
}
